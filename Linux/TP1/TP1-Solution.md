# TP1 : Are you dead yet ?

 **Trouver au moins 4 façons différentes de péter la machine**

--------------------------------------------------

_ Idée 1 : La commende clasique 
```
[robot-045@localhost ~]$ sudo rm -Rf /* 
[robot-045@localhost ~]$ reboot
```
Cette commande permet de supprimer tous les fichiers donc il n'y a plus rien.

_ Idée 2 : Identifiant
```
[robot-045@localhost ~]$ cd /
[robot-045@localhost ~]$ sudo rm bin
[robot-045@localhost ~]$ reboot
```
Cette méthode permet de supprimer les identifiants donc on ne peut se reconnecter au linux

_ Idée 3 : Plus de mot de passe  
```
[robot-045@localhost ~]$ cd /
[robot-045@localhost ~]$ cd etc
[robot-045@localhost ~]$ sudo rm passwd
[robot-045@localhost ~]$ exit
```
Mon vient supprimer le mot de passe donc on ne peut plus se connectée ceci dit on pourrait
le faire avec moins de commande par exemple avec la commande "sudo rm -r /etc/passwd" puis "exit".
Mais je voulais détailler le chemin pour être plus claire.


_ Idée 4 : Pourquoi ?
```
[robot-045@localhost ~]$ :(){ :|: & };:
```
EN tapant cette commande la machine ne répond plus on ne peut tout simplement plus rien faire.
Mais je ne sais pas pourquoi c'est google qui m'a proposé ça.

_ Idée 5 : Le barbare
```
[robot-045@localhost ~]$ cd /
[robot-045@localhost ~]$ cd etc
[robot-045@localhost ~]$ sudo nano shadow (supprimer tout)
[robot-045@localhost ~]$ reboot
```
Pour cette méthode on vient opéré directement dans un fichier précise en supprimant ce que contient
ce dernier pour casser la machine.

--------------------------------------------------

Quelques commandes qui peuvent faire le taff :

- `rm` (sur un seul fichier ou un petit groupe de fichiers)
- `nano` ou `vim` (sur un seul fichier ou un petit groupe de fichiers)
- `echo`
- `cat`
- `python`
- `systemctl`
- un script `bash`
- plein d'autres évidemment
