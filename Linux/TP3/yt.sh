#!/bin/bash
#09/12/2022 

video_title="$(youtube-dl -e $1)"
cd /srv/yt/downloads/
mkdir "${video_title}" > /dev/null
cd "${video_title}"
youtube-dl $1 > /dev/null
echo "Vidéo" $1 "was downloaded."
nom_vid="$(ls *.mp4)"
youtube-dl --get-description $1 > description
echo "File path : /srv/yt/downloads/""${video_title}""/""${nom_vid}"
cd /var/log/yt
echo "[""$(date "+%Y/%m/%d"" ""%T")""]" >> /var/log/yt/download.log
echo "Vidéo" $1 "was downloaded. File path : /srv/yt/downloads/""${video_title}""/""${nom_vid}" >> /var/log/yt/download.log