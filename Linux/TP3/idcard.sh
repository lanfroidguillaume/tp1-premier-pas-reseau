#!/bin/bash
#03/01/2023
#script carte d'identité

echo "Machine name : $(hostnamectl | grep hostname | cut -d':' -f2)"
echo "Os $(cat /etc/redhat-release | cut -d' ' -f-4) and kernel version is $(cat /etc/os-release | cut -d'=' -f2 | head -2 | tail -n1)"
echo "IP : $(ip a | grep enp0s8 | cut -d' ' -f6-8 | tail -n1)"
echo "RAM : $(free -h --mega | grep Mem: | tr -s ' ' | cut -d' ' -f7) memory available on $(free -h --mega | grep Mem: | tr -s ' ' | cut -d' ' -f2) total memory"
echo "Disk : $(df -h | tr -s ' ' | grep '/$' | cut -d' ' -f2) space left"

#top 5 des procsses comment faire :
echo "Top 5 processes by RAM usage :"
# aller cherhcer les processe en cours
processes="$(ps -o command= -e --sort -%mem | head -n5)"
# ecrire les precesses en allant a la ligne et mettre un - devant.
while read ligne ; do echo " - $ligne" ; done <<< "${processes}"

#listening port comment faire :
echo "Listening port :"
# aller chercher les listening port en cours :
listening="$(ss -ln4H | tr -s ' ' | cut -d':' -f2 | cut -d ' ' -f1) $(ss -ln4H | cut -d ' ' -f1) : $(sudo ss -ltnpu -4 | tr -s ' ' | cut -d' ' -f7 | tail -1 | cut -d'"' -f2)"
# ecrire les listening prot en allant a la ligne et mettre un - devant.
while read ligne ; do echo " - $ligne" ; done <<< "${listening}"

# Cat picture
# cat
cat_pic=$(curl https://cataas.com/cat > ./chate)
ext=$(file chate | tr -s ' ' | cut -d' ' -f2)
echo "Here is your random cat : ./cat.${ext}"