# TP 3 : We do a little scripting
# 0.Un premier script
```
[robot-045@localhost ~]$ cd /srv/
[robot-045@localhost srv]$ sudo nano test.sh
[robot-045@localhost srv]$ cat test.sh
#!/bin/bash
# Simple test script

echo "Connecté actuellement avec l'utilisateur $(whoami)."
[robot-045@localhost srv]$ ls -l
total 4
-rw-r--r--. 1 root root 94 Jan  3 11:06 test.sh
[robot-045@localhost srv]$ sudo chmod 744 test.sh
[robot-045@localhost srv]$ ls -l
total 4
-rwxr--r--. 1 root root 94 Jan  3 11:06 test.sh
[robot-045@localhost srv]$ sudo chmod +x test.sh
[robot-045@localhost srv]$ ./test.sh
Connecté actuellement avec l'utilisateur robot-045.
```
# I. Script carte d'identité
```
[robot-045@localhost srv]$ sudo mkdir idcard
[robot-045@localhost srv]$ ls
idcard  test.sh
[robot-045@localhost srv]$ cd idcard/
[robot-045@localhost idcard]$ sudo nano idcard.sh
[robot-045@localhost idcard]$ cat ./idcard.sh
#!/bin/bash
#03/01/2023
#script carte d'identité

echo "Machine name : $(hostnamectl | grep hostname | cut -d':' -f2)"
echo "Os $(cat /etc/redhat-release | cut -d' ' -f-4) and kernel version is $(cat /etc/os-release | cut -d'=' -f2 | head -2 | tail -n1)"
echo "IP : $(ip a | grep enp0s8 | cut -d' ' -f6-8 | tail -n1)"
echo "RAM : $(free -h --mega | grep Mem: | tr -s ' ' | cut -d' ' -f7) memory available on $(free -h --mega | grep Mem: | tr -s ' ' | cut -d' ' -f2) total memory"
echo "Disk : $(df -h | tr -s ' ' | grep '/$' | cut -d' ' -f2) space left"

#top 5 des procsses comment faire :
echo "Top 5 processes by RAM usage :"
# aller cherhcer les processe en cours
processes="$(ps -o command= -e --sort -%mem | head -n5)"
# ecrire les precesses en allant a la ligne et mettre un - devant.
while read ligne ; do echo " - $ligne" ; done <<< "${processes}"

#listening port comment faire :
echo "Listening port :"
# aller chercher les listening port en cours :
listening="$(ss -ln4H | tr -s ' ' | cut -d':' -f2 | cut -d ' ' -f1) $(ss -ln4H | cut -d ' ' -f1) : $(sudo ss -ltnpu -4 | tr -s ' ' | cut -d' ' -f7 | tail -1 | cut -d'"' -f2)"
# ecrire les listening prot en allant a la ligne et mettre un - devant.
while read ligne ; do echo " - $ligne" ; done <<< "${listening}"

# Cat picture
# cat
cat_pic=$(curl https://cataas.com/cat > ./chate)
ext=$(file chate | tr -s ' ' | cut -d' ' -f2)
echo "Here is your random cat : ./cat.${ext}"
[robot-045@localhost idcard]$ sudo chmod 744 idcard.sh
[robot-045@localhost idcard]$ sudo chmod +x idcard.sh
[robot-045@localhost idcard]$ ls -l
total 4
-rwxr-xr-x. 1 root root 50 Jan  3 11:32 idcard.sh
```
## Rendu
```
[robot-045@localhost idcard]$ ./idcard.sh
Machine name :  localhost.localdomain
Os Rocky Linux release 8.6 and kernel version is "8.6 (Green Obsidian)"
IP : 192.168.199.2/24 brd 192.168.199.255
RAM : 507M memory available on 804M total memory
Disk : 6.2G space left
Top 5 processes by RAM usage :
 - /usr/libexec/platform-python -s /usr/sbin/firewalld --nofork --nopid
 - /usr/libexec/sssd/sssd_nss --uid 0 --gid 0 --logger=files
 - /usr/libexec/platform-python -Es /usr/sbin/tuned -l -P
 - /usr/lib/polkit-1/polkitd --no-debug
 - /usr/sbin/NetworkManager --no-daemon
Listening port :
 - 22 tcp :
./idcard.sh: line 30: ./chate: Permission denied
Here is your random cat : ./cat.cannot
```

📁 Fichier /srv/idcard/idcard.sh
🌞 Vous fournirez dans le compte-rendu, en plus du fichier, un exemple d'exécution avec une sortie, dans des balises de code.

# II. Script youtube-dl
```
[robot-045@localhost srv]$ sudo mkdir yt
[robot-045@localhost srv]$ ls
idcard  test.sh  yt
[robot-045@localhost srv]$ cd yt
[robot-045@localhost yt]$ sudo nano yt.sh
[robot-045@localhost yt]$ cat yt.sh
#!/bin/bash
#09/12/2022

video_title="$(youtube-dl -e $1)"
cd /srv/yt/downloads/
mkdir "${video_title}" > /dev/null
cd "${video_title}"
youtube-dl $1 > /dev/null
echo "Vidéo" $1 "was downloaded."
nom_vid="$(ls *.mp4)"
youtube-dl --get-description $1 > description
echo "File path : /srv/yt/downloads/""${video_title}""/""${nom_vid}"
cd /var/log/yt
echo "[""$(date "+%Y/%m/%d"" ""%T")""]" >> /var/log/yt/download.log
echo "Vidéo" $1 "was downloaded. File path : /srv/yt/downloads/""${video_title}""/""${nom_vid}" >> /var/log/yt/download.log
[robot-045@localhost yt]$ sudo chmod 744 yt.sh
[robot-045@localhost yt]$ sudo chmod +x yt.sh
[robot-045@localhost yt]$ ls -l
total 4
-rwxr-xr-x. 1 root root 12 Jan  9 15:38 yt.sh
[robot-045@localhost yt]$ sudo mkdir downloads.log
```

## Rendu

```
[robot-045@localhost yt]$ bash yt.sh https://www.youtube.com/watch?v=sNx57atloH8
Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded.
File path : /srv/yt/downloads/tomato anxiety/tomato anxiety.mp4
```
```
[robot-045@localhost yt]$ cat /var/log/yt/download.log
[16/12/22 16:45:24] Video https://www.youtube.com/watch?v=sNx57atloH8 was downloaded. File path : /srv/yt/downloads/tomato>
[16/12/22 17:04:16] Video https://www.youtube.com/watch?v=D56HJp7dRFw was downloaded. File path : /srv/yt/downloads/Bob>
```

📁 Le script /srv/yt/yt.sh
📁 Le fichier de log /var/log/yt/download.log, avec au moins quelques lignes
🌞 Vous fournirez dans le compte-rendu, en plus du fichier, un exemple d'exécution avec une sortie, dans des balises de code.

# III. MAKE IT A SERVICE

```
```

## Rendu

```
```

📁 **Le script `/srv/yt/yt-v2.sh`**

📁 **Fichier `/etc/systemd/system/yt.service`**

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :
