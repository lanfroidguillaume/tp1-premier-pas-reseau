# TP4 : Real services
# Partie 1 : Partitionnement du serveur de stockage

```
[robot-045@localhost ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sda           8:0    0    8G  0 disk
├─sda1        8:1    0    1G  0 part /boot
└─sda2        8:2    0    7G  0 part
  ├─rl-root 253:0    0  6.2G  0 lvm  /
  └─rl-swap 253:1    0  820M  0 lvm  [SWAP]
sdb           8:16   0    2G  0 disk
sr0          11:0    1 1024M  0 rom
[robot-045@localhost ~]$ sudo pvcreate /dev/sdb
[robot-045@localhost ~]$ sudo pvs
  PV         VG Fmt  Attr PSize  PFree
  /dev/sda2  rl lvm2 a--  <7.00g    0
  /dev/sdb      lvm2 ---   2.00g 2.00g
[robot-045@localhost ~]$ sudo pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda2
  VG Name               rl
  PV Size               <7.00 GiB / not usable 3.00 MiB
  Allocatable           yes (but full)
  PE Size               4.00 MiB
  Total PE              1791
  Free PE               0
  Allocated PE          1791
  PV UUID               9J1q0P-pGcv-VabP-i178-efmJ-dTu7-cQKQdg

  "/dev/sdb" is a new physical volume of "2.00 GiB"
  --- NEW Physical volume ---
  PV Name               /dev/sdb
  VG Name
  PV Size               2.00 GiB
  Allocatable           NO
  PE Size               0
  Total PE              0
  Free PE               0
  Allocated PE          0
  PV UUID               SDWSCU-bRSX-kryW-02x2-SSYg-oKOr-dw9Xyk
[robot-045@localhost ~]$ sudo vgcreate storage /dev/sdb
  Volume group "storage" successfully created
  [robot-045@localhost ~]$ sudo vgs
  VG      #PV #LV #SN Attr   VSize  VFree
  rl        1   2   0 wz--n- <7.00g     0
  storage   1   0   0 wz--n- <2.00g <2.00g
[robot-045@localhost ~]$ sudo vgdisplay
  --- Volume group ---
  VG Name               storage
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  1
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                0
  Open LV               0
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <2.00 GiB
  PE Size               4.00 MiB
  Total PE              511
  Alloc PE / Size       0 / 0
  Free  PE / Size       511 / <2.00 GiB
  VG UUID               4uRamD-3F4f-7Biq-Wkjg-K0jC-014P-ccS1Yt

  --- Volume group ---
  VG Name               rl
  System ID
  Format                lvm2
  Metadata Areas        1
  Metadata Sequence No  3
  VG Access             read/write
  VG Status             resizable
  MAX LV                0
  Cur LV                2
  Open LV               2
  Max PV                0
  Cur PV                1
  Act PV                1
  VG Size               <7.00 GiB
  PE Size               4.00 MiB
  Total PE              1791
  Alloc PE / Size       1791 / <7.00 GiB
  Free  PE / Size       0 / 0
  VG UUID               9Q8bvV-ETY8-PytO-0DFc-TorE-zt06-M6F1jN
```
```
[robot-045@localhost ~]$ sudo lvcreate -l 100%FREE storage -n lvdisplay
  Logical volume "lvdisplay" created.
[robot-045@localhost ~]$ sudo lvs
  LV        VG      Attr       LSize   Pool Origin Data%  Meta%  Move Log Cpy%Sync Convert
  root      rl      -wi-ao----  <6.20g

  swap      rl      -wi-ao---- 820.00m

  lvdisplay storage -wi-a-----  <2.00g

[robot-045@localhost ~]$ sudo lvdisplay
  --- Logical volume ---
  LV Path                /dev/storage/lvdisplay
  LV Name                lvdisplay
  VG Name                storage
  LV UUID                9Nsgs9-3LaJ-kY27-lyCf-wb0Y-B0mm-YNyK84
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2023-01-10 17:03:20 +0100
  LV Status              available
  # open                 0
  LV Size                <2.00 GiB
  Current LE             511
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:2

  --- Logical volume ---
  LV Path                /dev/rl/swap
  LV Name                swap
  VG Name                rl
  LV UUID                BAYwyq-FWdm-s9uV-I0YY-Llq2-Xcj1-TzklI2
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2022-10-13 12:28:10 +0200
  LV Status              available
  # open                 2
  LV Size                820.00 MiB
  Current LE             205
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:1

  --- Logical volume ---
  LV Path                /dev/rl/root
  LV Name                root
  VG Name                rl
  LV UUID                Pj0bcy-s8rR-wpeB-fk1c-NLhi-micI-99Q5zS
  LV Write Access        read/write
  LV Creation host, time localhost.localdomain, 2022-10-13 12:28:10 +0200
  LV Status              available
  # open                 1
  LV Size                <6.20 GiB
  Current LE             1586
  Segments               1
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     8192
  Block device           253:0
```
```
[robot-045@localhost ~]$ sudo mkfs -t ext4 /dev/storage/lvdisplay
mke2fs 1.45.6 (20-Mar-2020)
Creating filesystem with 523264 4k blocks and 130816 inodes
Filesystem UUID: e6afa910-5ce2-45a3-8577-275b0204f89f
Superblock backups stored on blocks:
        32768, 98304, 163840, 229376, 294912

Allocating group tables: done
Writing inode tables: done
Creating journal (8192 blocks): done
Writing superblocks and filesystem accounting information: done
[robot-045@localhost ~]$ sudo mkdir /mnt/storage
[robot-045@localhost ~]$ sudo mount /dev/storage/lvdisplay /mnt/storage
[robot-045@localhost ~]$ df -h | grep storage
/dev/mapper/storage-lvdisplay  2.0G  6.0M  1.9G   1% /mnt/storage
```
```
[robot-045@localhost ~]$ sudo nano /etc/fstab
[robot-045@localhost ~]$ cat /etc/fstab
[...]
/dev/storage/lvdisplay /mnt/storage ext4 defaults 0 0
[robot-045@localhost ~]$ sudo umount /mnt/storage
[robot-045@localhost ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
mount: /mnt/storage does not contain SELinux labels.
       You just mounted an file system that supports labels which does not
       contain labels, onto an SELinux box. It is likely that confined
       applications will generate AVC messages and not be allowed access to
       this file system.  For more details see restorecon(8) and mount(8).
/mnt/storage             : successfully mounted
[robot-045@localhost ~]$ sudo mount -av
/                        : ignored
/boot                    : already mounted
none                     : ignored
/mnt/storage             : already mounted
[robot-045@localhost ~]$ sudo reboot
Connection to 192.168.199.2 closed by remote host.
Connection to 192.168.199.2 closed.
``` 

# Partie 2 : Serveur de partage de fichiers

-> server
``` 
[robot-045@localhost ~]$ cd storage/
[robot-045@localhost storage]$ sudo mkdir site_web_1
[robot-045@localhost storage]$ sudo mkdir site_web_2
[robot-045@localhost storage]$ ls
site_web_1  site_web_2
[robot-045@localhost storage]$ sudo mkdir /var/nfs/general -p
[robot-045@localhost storage]$ ls -dl /var/nfs/general
drwxr-xr-x. 2 root root 6 Jan 16 15:51 /var/nfs/general
[robot-045@localhost storage]$ sudo chown nobody /var/nfs/general
[robot-045@localhost storage]$ sudo dnf install nano
[robot-045@localhost storage]$ sudo nano /etc/exports
[robot-045@localhost storage]$ cat /etc/exports
#16/01/2023
/storage/site_web_1/ 192.168.199.20(rw,sync,no_subtree_check)
/storage/site_web_2/ 192.168.199.20(rw,sync,no_subtree_check)
[robot-045@localhost ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
[robot-045@localhost ~]$ sudo systemctl start nfs-server
[robot-045@localhost ~]$ sudo systemctl status nfs-server
● nfs-server.service - NFS server and services
   Loaded: loaded (/usr/lib/systemd/system/nfs-server.service; enabled; vendor pre>
  Drop-In: /run/systemd/generator/nfs-server.service.d
           └─order-with-mounts.conf
   Active: active (exited) since Mon 2023-01-16 17:26:04 CET; 1min 58s ago
  Process: 24238 ExecStart=/bin/sh -c if systemctl -q is-active gssproxy; then sys>
  Process: 24226 ExecStart=/usr/sbin/rpc.nfsd (code=exited, status=0/SUCCESS)
  Process: 24224 ExecStartPre=/usr/sbin/exportfs -r (code=exited, status=1/FAILURE)
 Main PID: 24238 (code=exited, status=0/SUCCESS)

Jan 16 17:26:03 localhost.localdomain systemd[1]: Starting NFS server and services>
Jan 16 17:26:03 localhost.localdomain exportfs[24224]: exportfs: Failed to stat /s>
[robot-045@localhost ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client ssh
[robot-045@localhost ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[robot-045@localhost ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[robot-045@localhost ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[robot-045@localhost ~]$ sudo firewall-cmd --reload
success
[robot-045@localhost ~]$ sudo firewall-cmd --permanent --list-all | grep services
  services: cockpit dhcpv6-client mountd nfs rpc-bind ssh
``` 

-> client
```
[robot-045@localhost ~]$ sudo dnf install nfs-utils
[robot-045@localhost ~]$ sudo mkdir -p /nfs/general
[robot-045@localhost ~]$ sudo mkdir -p /nfs/home
[robot-045@localhost ~]$ sudo mkdir /var/www/site_web_1 -p
[robot-045@localhost ~]$ sudo mkdir /var/www/site_web_2 -p
[robot-045@localhost ~]$ ls -la /var/www/
total 4
drwxr-xr-x.  4 root root   42 Jan 16 20:57 .
drwxr-xr-x. 22 root root 4096 Jan 16 20:57 ..
drwxr-xr-x.  2 root root    6 Jan 16 20:57 site_web_1
drwxr-xr-x.  2 root root    6 Jan 16 20:57 site_web_2
[robot-045@localhost ~]$ sudo mount 192.168.199.19:/storage/site_web_1 /var/ www/site_web_1
[robot-045@localhost ~]$ sudo mount 192.168.199.19:/storage/site_web_2 /var/ www/site_web_2
[robot-045@localhost ~]$ df -h | grep storage
10.4.1.20:/storage/site_web_1  2.0G     0  1.9G   0% /var/www/site_web_1
10.4.1.20:/storage/site_web_2  2.0G     0  1.9G   0% /var/www/site_web_2
[robot-045@localhost ~]$ sudo nano /etc/fstab
[robot-045@localhost ~]$ cat /etc/fstab | grep storage
192.168.199.19:/storage/site_web_1 /var/www/site_web_1 nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
192.168.199.19:/storage/site_web_2 /var/www/site_web_2 nfs auto,nofail,noatime,nolock,intr,tcp,actimeo=1800 0 0
[robot-045@localhost ~]$ sudo umount /var/www/site_web_1
[robot-045@localhost ~]$ sudo umount /var/www/site_web_2
[robot-045@localhost ~]$ df -h | grep storage
```

# Partie 3 : Serveur web
🌞 Installez NGINX
```
[robot-045@localhost ~]$ sudo dnf install nginx
[robot-045@localhost ~]$ sudo systemctl enable nginx
Created symlink /etc/systemd/system/multi-user.target.wants/nginx.service → /usr/lib/systemd/system/nginx.service.
[robot-045@localhost ~]$ sudo systemctl start nginx
```

🌞 Analysez le service NGINX
```
[robot-045@localhost ~]$ ps -ef | grep nginx
lukas      11723    1188  0 15:26 pts/0    00:00:00 grep --color=auto nginx
[lukas@web ~]$ sudo ss -alnpt4 | grep nginx
LISTEN 0      511          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=11780,fd=6),("nginx",pid=11779,fd=6))
[robot-045@localhost ~]$ cat /etc/nginx/nginx.conf | grep /html
        root         /usr/share/nginx/html;
#        root         /usr/share/nginx/html;
```

🌞 Configurez le firewall pour autoriser le trafic vers le service NGINX
```
[robot-045@localhost ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[robot-045@localhost ~]$ sudo firewall-cmd --reload
success
[robot-045@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp 80/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
  ```

🌞 Accéder au site web
```
[robot-045@localhost ~]$ curl http://10.4.1.20 | head
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/
```

🌞 Vérifier les logs d'accès
```
[robot-045@localhost ~]$ sudo cat /var/log/nginx/access.log | tail -n 3
10.4.1.1 - - [10/Jan/2023:17:53:45 +0100] "GET /icons/poweredby.png HTTP/1.1" 200 15443 "http://10.4.1.20/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0" "-"
10.4.1.1 - - [10/Jan/2023:17:53:45 +0100] "GET /poweredby.png HTTP/1.1" 200 368 "http://10.4.1.20/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0" "-"
10.4.1.1 - - [10/Jan/2023:17:53:45 +0100] "GET /favicon.ico HTTP/1.1" 404 3332 "http://10.4.1.20/" "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/106.0.0.0 Safari/537.36 OPR/92.0.0.0" "-"
```

🌞 Changer le port d'écoute
```
[robot-045@localhost ~]$ sudo nano /etc/nginx/nginx.conf
        listen       8080;
        listen       [::]:8080
[robot-045@localhost ~]$ sudo systemctl restart nginx
[robot-045@localhost ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
     Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor pre>
     Active: active (running) since Tue 2023-01-10 17:57:10 CET; 21s ago
    Process: 11874 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, sta>
    Process: 11875 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCE>
    Process: 11876 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
   Main PID: 11877 (nginx)
      Tasks: 2 (limit: 4638)
     Memory: 1.9M
        CPU: 11ms
     CGroup: /system.slice/nginx.service
             ├─11877 "nginx: master process /usr/sbin/nginx"
             └─11878 "nginx: worker process"
[robot-045@localhost ~]$ sudo ss -alnpt4 | grep nginx
LISTEN 0      511          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=11878,fd=6),("nginx",pid=11877,fd=6))
[robot-045@localhost ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[robot-045@localhost ~]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[robot-045@localhost ~]$ sudo firewall-cmd --reload
success
[robot-045@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 22/tcp 8080/tcp
  protocols:
  forward: yes
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[robot-045@localhost ~]$ curl http://10.4.1.20:8080 | head -n 7
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
```

🌞 Changer l'utilisateur qui lance le service
```
[robot-045@localhost ~]$ sudo useradd web -m
[robot-045@localhost ~]$ sudo passwd web
New password:
Retype new password:
passwd: all authentication tokens updated successfully.
[robot-045@localhost ~]$ sudo nano /etc/nginx/nginx.conf
[robot-045@localhost ~]$ cat /etc/nginx/nginx.conf | grep web
user web;
[robot-045@localhost ~]$ sudo systemctl restart nginx
[robot-045@localhost ~]$ ps -ef | grep web | grep nginx
web        12019   12018  0 16:04 ?        00:00:00 nginx: worker process
```

🌞 Changer l'emplacement de la racine Web
```
[robot-045@localhost ~]$ sudo nano /var/www/site_web_1/index.html
<h1>Hello There !</h1>
[robot-045@localhost ~]$ sudo nano /etc/nginx/nginx.conf
[robot-045@localhost ~]$ cat /etc/nginx/nginx.conf | grep root
        root         /var/www/site_web_1/;
[robot-045@localhost ~]$ sudo systemctl restart nginx
[robot-045@localhost ~]$ curl http://10.4.1.20:8080
<h1>Hello There !</h1>
```

🌞 Repérez dans le fichier de conf
```
[robot-045@localhost ~]$ cat /etc/nginx/nginx.conf | grep /conf.d/
    include /etc/nginx/conf.d/*.conf;
```

🌞 Créez le fichier de configuration pour le premier site
```
[robot-045@localhost ~]$ sudo nano /etc/nginx/conf.d/site_web_1.conf
    server {
        listen       8080;
        listen       [::]:80;
        server_name  _;
        root         /var/www/site_web_1/;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
```

🌞 Créez le fichier de configuration pour le deuxième site
```
[robot-045@localhost ~]$ sudo nano /etc/nginx/conf.d/site_web_2.conf
    server {
        listen       8888;
        listen       [::]:80;
        server_name  _;
        root         /var/www/site_web_2/;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }
[robot-045@localhost ~]$ sudo firewall-cmd --add-port=8888/tcp --permanent
success
[robot-045@localhost ~]$ sudo firewall-cmd --reload
success
```

🌞 Prouvez que les deux sites sont disponibles
```
[robot-045@localhost ~]$ curl http://10.4.1.20:8080
<h1>Hello There !</h1>
[robot-045@localhost ~]$ curl http://10.4.1.20:8888
<h1>I'm the boss</h1>
```
