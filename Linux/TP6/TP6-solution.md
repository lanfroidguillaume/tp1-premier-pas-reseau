# TP6 : Travail autour de la solution NextCloud

# Module 1 : Reverse Proxy
### I. Setup
🌞 On utilisera NGINX comme reverse proxy
```
[robot-045@proxy ~]$ sudo dnf install nginx -y
[robot-045@proxy ~]$ sudo systemctl start nginx
[robot-045@proxy ~]$ sudo systemctl enable nginx
[robot-045@proxy ~]$ sudo systemctl status nginx
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; enabled; vendor>
   Active: active (running) since Tue 2023-02-07 07:14:27 CET; 1min 40s a>
 Main PID: 4219 (nginx)
    Tasks: 3 (limit: 4916)
   Memory: 5.7M
   CGroup: /system.slice/nginx.service
           ├─4219 nginx: master process /usr/sbin/nginx
           ├─4220 nginx: worker process
           └─4221 nginx: worker process
[robot-045@proxy ~]$ sudo netstat -pnltu | grep nginx
tcp        0      0 0.0.0.0:80              0.0.0.0:*               LISTEN      4219/nginx: master
tcp6       0      0 :::80                   :::*                    LISTEN      4219/nginx: master
[robot-045@proxy ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[robot-045@proxy ~]$ sudo firewall-cmd --reload
success
[robot-045@proxy ~]$ ps -ef | grep nginx
root        4219       1  0 07:14 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       4220    4219  0 07:14 ?        00:00:00 nginx: worker process
nginx       4221    4219  0 07:14 ?        00:00:00 nginx: worker process
robot-0+    4290    1674  0 07:19 pts/0    00:00:00 grep --color=auto nginx
[robot-045@proxy ~]$ curl 10.105.1.14:80 | grep nginx
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3429  100  3429    0     0  1116k      0 --:--:-- --:--:-- --:--:-- 1674k
    <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>
        <strong>nginx</strong> HTTP server after it has been installed. If you
            with <strong>nginx</strong> on Rocky Linux. It is located in
            <tt>/usr/share/nginx/html</tt>.
            <strong>nginx</strong>
            <tt>/etc/nginx/nginx.conf</tt>.
        <a href="http://nginx.net/"
            src="nginx-logo.png"
            alt="[ Powered by nginx ]"
```

🌞 Configurer NGINX
```
[robot-045@proxy ~]$ sudo nano /etc/nginx/conf.d/reverse.conf
[robot-045@proxy ~]$ cat /etc/nginx/conf.d/reverse.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veu>    server_name web.tp6.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11:80;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
```
[robot-045@web ~]$ sudo nano /etc/nginx/conf.d/reverse.conf
[robot-045@web ~]$ cat /etc/nginx/conf.d/reverse.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp5.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
}
```
🌞 Faites en sorte de
```
[robot-045@web ~]$ sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source address="10.105.1.14" accept
'
success
[robot-045@web ~]$ sudo firewall-cmd --permanent --zone=public --add-rich-rule='rule family="ipv4" source not address="10.105.1.14" dr
op'
success
[robot-045@web ~]$ sudo firewall-cmd --reload
success
```

🌞 Une fois que c'est en place
```
PS C:\Users\lanfr> ping 10.105.1.14

Envoi d’une requête 'Ping'  10.105.1.14 avec 32 octets de données :
Réponse de 10.105.1.14 : octets=32 temps<1ms TTL=64
Réponse de 10.105.1.14 : octets=32 temps=1 ms TTL=64
Réponse de 10.105.1.14 : octets=32 temps=1 ms TTL=64
Réponse de 10.105.1.14 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 10.105.1.14:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 1ms, Moyenne = 0ms
PS C:\Users\lanfr> ping 10.105.1.11

Envoi d’une requête 'Ping'  10.105.1.11 avec 32 octets de données :
Réponse de 10.105.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.105.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.105.1.11 : octets=32 temps<1ms TTL=64
Réponse de 10.105.1.11 : octets=32 temps<1ms TTL=64

Statistiques Ping pour 10.105.1.11:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 0ms, Maximum = 0ms, Moyenne = 0ms
```

### II. HTTPS
🌞 Faire en sorte que NGINX force la connexion en HTTPS plutôt qu'HTTP
```
[robot-045@proxy ~]$ sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx.key -out /etc/ssl/certs/certificate
Generating a RSA private key
....................+++++
.......................................................+++++
writing new private key to '/etc/ssl/private/nginx.key'
req: Can't open "/etc/ssl/private/nginx.key" for writing, No such file or directory
[robot-045@proxy ~]$ sudo nano /etc/nginx/conf.d/reverse.conf
[robot-045@proxy ~]$ cat /etc/nginx/conf.d/reverse.conf
server {
    # On indique le nom que client va saisir pour accéder au service
    # Pas d'erreur ici, c'est bien le nom de web, et pas de proxy qu'on veut ici !
    server_name web.tp5.linux;

    # Port d'écoute de NGINX
    listen 80;

    location / {
        # On définit des headers HTTP pour que le proxying se passe bien
        proxy_set_header  Host $host;
        proxy_set_header  X-Real-IP $remote_addr;
        proxy_set_header  X-Forwarded-Proto https;
        proxy_set_header  X-Forwarded-Host $remote_addr;
        proxy_set_header  X-Forwarded-For $proxy_add_x_forwarded_for;

        # On définit la cible du proxying
        proxy_pass http://10.105.1.11;
    }

    # Deux sections location recommandés par la doc NextCloud
    location /.well-known/carddav {
      return 301 $scheme://$host/remote.php/dav;
    }

    location /.well-known/caldav {
      return 301 $scheme://$host/remote.php/dav;
    }
    listen 443 http2 ssl;
    listen [::]:443 http2 ssl;
    ssl_certificate /etc/ssl/certs/certificate;
    ssl_certificate_key /etc/ssl/private/nginx.key;
}
[robot-045@proxy ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[robot-045@proxy ~]$ sudo firewall-cmd --reload
success
[robot-045@proxy ~]$ sudo systemctl restart nginx
```

# Module 2 : Sauvegarde du système de fichiers
### I. Script de backup
**I. Ecriture du script**
🌞 Ecrire le script bash
```
[robot-045@web ~]$ sudo nano /srv/tp6_backup.sh
[robot-045@web ~]$ cat /srv/tp6_backup.sh
#!/bin/bash
# 12/02/2023
# Créer une backup

sed -i "s/'maintenance' => false,/'maintenance' => true,/" /var/www/tp5_nextcloud/config/config.php
mkdir -p /srv/backup
cd /srv/backup/
heure="$(date +%H%M)"
jour="$(date +%y%m%d)"
name="nextcloud_$date.zip"
zip $name /var/www/tp5_nextcloud/>/dev/null
echo "Backup effectué !"
sed -i "s/'maintenance' => true,/'maintenance' => false,/" /var/www/tp5_nextcloud/config/config.php
```

**II. Clean it**
```
[robot-045@web ~]$ sudo useradd -m -d /srv/backup/ -s /usr/sbin/nologin backup
[robot-045@web ~]$ sudo useradd -D  -b /srv/backup/ -g 1000 -s /usr/bin/nologin
[robot-045@web ~]$ sudo useradd -D  -b /srv/backup/ -s /usr/bin/nologin
```
```
[robot-045@web ~]$ sudo -u backup /srv/tp6_backup.sh
```
**III. Service et timer**
🌞 Créez un service système qui lance le script
```
[robot-045@web ~]$ sudo nano /etc/systemd/system/backup.service
[robot-045@web ~]$ cat /etc/systemd/system/backup.service
[Service]
Type=oneshot
ExecStart=/srv/tp6_backup.sh start

[robot-045@web ~]$ sudo systemctl status backup
○ backup.service
     Loaded: loaded (/etc/systemd/system/backup.service; static)
     Active: inactive (dead)
[robot-045@web ~]$ sudo systemctl start backup
[robot-045@web ~]$ sudo systemctl status backup
○ backup.service
     Loaded: loaded (/etc/systemd/system/backup.service; static)
     Active: inactive (dead)

Feb 12 12:24:12 web systemd[1]: Starting backup.service...
Feb 12 12:24:12 web tp6_backup.sh[12507]: Backup effectué !
Feb 12 12:24:12 web systemd[1]: backup.service: Deactivated successfully.
Feb 12 12:24:12 web systemd[1]: Finished backup.service.
```

🌞 Créez un timer système qui lance le service à intervalles réguliers
```
[robot-045@web ~]$ sudo nano /etc/systemd/system/backup.timer
[robot-045@web ~]$ cat /etc/systemd/system/backup.timer
[Unit]
Description=Run backup service

[Timer]
OnCalendar=*-*-* 4:00:00

[Install]
WantedBy=timers.target
```

🌞 Activez l'utilisation du timer
```
[robot-045@web ~]$ sudo systemctl daemon-reload
[robot-045@web ~]$ sudo systemctl start backup.timer
[robot-045@web ~]$ sudo systemctl enable backup.timer
Created symlink /etc/systemd/system/timers.target.wants/backup.timer → /etc/systemd/system/backup.timer.
[robot-045@web ~]$ sudo systemctl status backup.timer
● backup.timer - Run backup service
   Loaded: loaded (/etc/systemd/system/backup.timer; enabled; vendor preset: disab>
   Active: active (waiting) since Sun 2023-02-12 11:27:00 CET; 18s ago
  Trigger: Mon 2023-02-13 04:00:00 CET; 16h left

Feb 12 11:27:00 web systemd[1]: Started Run backup service.

[robot-045@web ~]$ sudo systemctl list-timers
NEXT                         LEFT       LAST                         PASSED    UNI>
Sun 2023-02-12 11:52:37 CET  24min left n/a                          n/a       dnf>
Mon 2023-02-13 00:00:00 CET  12h left   Sun 2023-02-12 10:59:08 CET  28min ago mlo>
Mon 2023-02-13 00:00:00 CET  12h left   Sun 2023-02-12 10:59:08 CET  28min ago unb>
Mon 2023-02-13 04:00:00 CET  16h left   n/a                          n/a       bac>
Mon 2023-02-13 11:14:02 CET  23h left   Sun 2023-02-12 11:14:02 CET  13min ago sys>

```

### II. NFS
**I. Serveur NFS**
🌞 Préparer un dossier à partager sur le réseau (sur la machine storage.tp6.linux)
```
[robot-045@storage ~]$ sudo mkdir /srv/nfs_shares
[robot-045@storage ~]$ sudo chown nobody /srv/nfs_shares/
[robot-045@storage ~]$ sudo mkdir /srv/nfs_shares/web.tp6.linux
[robot-045@storage ~]$ sudo chown nobody /srv/nfs_shares/web.tp6.linux/
```

🌞 Installer le serveur NFS (sur la machine storage.tp6.linux)
```
[robot-045@storage ~]$ sudo dnf install nfs-utils
[robot-045@storage ~]$ sudo nano /etc/exports
/srv/nfs_shares/web.tp6.linux/    10.105.1.11(rw,sync,no_subtree_check)
[robot-045@storage ~]$ sudo firewall-cmd --permanent --add-service=nfs
success
[robot-045@storage ~]$ sudo firewall-cmd --permanent --add-service=mountd
success
[robot-045@storage ~]$ sudo firewall-cmd --permanent --add-service=rpc-bind
success
[robot-045@storage ~]$ sudo firewall-cmd --reload
success
[robot-045@storage ~]$ sudo firewall-cmd --list-all | grep services
  services: cockpit dhcpv6-client mountd nfs rpc-bind ssh
[robot-045@storage ~]$ sudo systemctl start nfs-server
[robot-045@storage ~]$ sudo systemctl status nfs-server | grep Active
     Active: active (exited) since Tue 2023-02-12 12:44:30 CET; 35s ago
```

**II. Client NFS**
🌞 Installer un client NFS sur web.tp6.linux
```
[robot-045@web ~]$ sudo mount 10.105.1.14:/srv/nfs_shares/web.tp6.linux/ /srv/backup/
[robot-045@web ~]$df -h | grep /srv/backup
10.105.1.14:/srv/nfs_shares/web.tp6.linux  6.2G  1.3G  4.9G  21%  /srv/backup 

```

🌞 Tester la restauration des données sinon ça sert à rien :)
```

```


# Module 3 : Fail2Ban

🌞 Faites en sorte que :
- si quelqu'un se plante 3 fois de password pour une co SSH en moins de 1 minute, il est ban
- vérifiez que ça fonctionne en vous faisant ban
- utilisez une commande dédiée pour lister les IPs qui sont actuellement ban
- afficher l'état du firewal, et trouver la ligne qui ban l'IP en question
- lever le ban avec une commande liée à fail2ban

```
[robot-045@db ~]$ sudo dnf install epel-release
[robot-045@db ~]$ sudo dnf install fail2ban fail2ban-firewalld
[robot-045@db ~]$ sudo systemctl start fail2ban
[robot-045@db ~]$ sudo systemctl enable fail2ban
[robot-045@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; >
   Active: active (running) since Tue 2023-02-07 11:20:53 CET; 27s ago
     Docs: man:fail2ban(1)
 Main PID: 13278 (fail2ban-server)
    Tasks: 3 (limit: 4916)
   Memory: 12.3M
   CGroup: /system.slice/fail2ban.service
           └─13278 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf >
[robot-045@db ~]$ sudo cp /etc/fail2ban/jail.conf /etc/fail2ban/jail.local

[robot-045@db ~]$ sudo nano /etc/fail2ban/jail.local

[robot-045@db ~]$ sudo cat /etc/fail2ban/jail.local | grep findtime
# A host is banned if it has generated "maxretry" during the last "findtime"
findtime  = 1m

[robot-045@db ~]$ sudo cat /etc/fail2ban/jail.local | grep maxretry
# A host is banned if it has generated "maxretry" during the last "findtime"
# "maxretry" is the number of failures before a host get banned.
maxretry = 3

[robot-045@db ~]$ sudo systemctl status fail2ban
● fail2ban.service - Fail2Ban Service
   Loaded: loaded (/usr/lib/systemd/system/fail2ban.service; enabled; vendor preset: disab>
   Active: active (running) since Tue 2023-02-07 11:20:53 CET; 5min ago
     Docs: man:fail2ban(1)
 Main PID: 13278 (fail2ban-server)
    Tasks: 3 (limit: 4916)
   Memory: 12.3M
   CGroup: /system.slice/fail2ban.service
           └─13278 /usr/bin/python3.6 -s /usr/bin/fail2ban-server -xf start

Feb 07 11:20:53 db systemd[1]: Starting Fail2Ban Service...
Feb 07 11:20:53 db systemd[1]: Started Fail2Ban Service.
Feb 07 11:20:53 db fail2ban-server[13278]: Server ready

[robot-045@web ~]$ ssh robot-045@10.105.1.12
robot-045@10.105.1.12's password:
Permission denied, please try again.
robot-045@10.105.1.12's password:
Permission denied, please try again.
robot-045@10.105.1.12's password:
robot-045@10.105.1.12: Permission denied (publickey,gssapi-keyex,gssapi-with-mic,password).
[robot-045@web ~]$ ssh mat@10.105.1.12
ssh: connect to host 10.105.1.12 port 22: Connection refused

[robot-045@db ~]$ sudo fail2ban-client status sshd | grep Banned
    `- Banned IP list:   10.105.1.11
[robot-045@db ~]$ sudo firewall-cmd --list-all | grep rule
  rich rules:
        rule family="ipv4" source address="10.105.1.11" port port="ssh" protocol="tcp" reject type="icmp-port-unreachable"
[robot-045@db ~]$ sudo fail2ban-client unban 10.105.1.11
1
```

# Module 4 : Monitoring

🌞 Installer Netdata
```
[robot-045@web ~]$ bash <(curl -Ss https://my-netdata.io/kickstart.sh)
[robot-045@web ~]$ sudo systemctl start netdata
[robot-045@web ~]$ sudo systemctl enable netdata
[robot-045@web ~]$ sudo systemctl status netdata
● netdata.service - Real time performance monitoring
   Loaded: loaded (/usr/lib/systemd/system/netdata.service; enabled; vendor preset: disabled)
   Active: active (running) since Sun 2023-02-05 17:37:57 CET; 5min ago
 Main PID: 2291 (netdata)
    Tasks: 70 (limit: 4916)
   Memory: 191.4M
   CGroup: /system.slice/netdata.service
           ├─2291 /usr/sbin/netdata -P /run/netdata/netdata.pid -D
           ├─2295 /usr/sbin/netdata --special-spawn-server
           ├─2817 /usr/libexec/netdata/plugins.d/go.d.plugin 1
           ├─2818 /usr/libexec/netdata/plugins.d/ebpf.plugin 1
           └─2820 /usr/libexec/netdata/plugins.d/apps.plugin 1
[robot-045@web ~]$ sudo netstat -pnltu | grep netdata
tcp        0      0 127.0.0.1:8125          0.0.0.0:*               LISTEN      2291/netdata
tcp        0      0 0.0.0.0:19999           0.0.0.0:*               LISTEN      2291/netdata
tcp6       0      0 ::1:8125                :::*                    LISTEN      2291/netdata
tcp6       0      0 :::19999                :::*                    LISTEN      2291/netdata
udp        0      0 127.0.0.1:8125          0.0.0.0:*                           2291/netdata
udp6       0      0 ::1:8125                :::*                                2291/netdata
[robot-045@web ~]$ sudo firewall-cmd --permanent --add-port=19999/tcp
success
[robot-045@web ~]$  sudo firewall-cmd --reload
success
```

🌞 Une fois Netdata installé et fonctionnel, déterminer :
l'utilisateur sous lequel tourne le(s) processus Netdata
```
[robot-045@web ~]$ ps -ef | grep netdata | head -n5 | tail -n-1
netdata     2817    2291  2 17:37 ?        00:00:13 /usr/libexec/netdata/plugins.d/go.d.plugin 1
```
si Netdata écoute sur des ports
```
[robot-045@web ~]$ sudo netstat -pnltu | grep netdata
tcp        0      0 127.0.0.1:8125          0.0.0.0:*               LISTEN      2291/netdata
tcp        0      0 0.0.0.0:19999           0.0.0.0:*               LISTEN      2291/netdata
tcp6       0      0 ::1:8125                :::*                    LISTEN      2291/netdata
tcp6       0      0 :::19999                :::*                    LISTEN      2291/netdata
udp        0      0 127.0.0.1:8125          0.0.0.0:*                           2291/netdata
udp6       0      0 ::1:8125                :::*                                2291/netdata
```
comment sont consultables les logs de Netdata
```
[robot-045@web ~]$ sudo ss -ltpnu | grep netdata
udp   UNCONN 0      0          127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2291,fd=48))

udp   UNCONN 0      0              [::1]:8125          [::]:*    users:(("netdata",pid=2291,fd=41))

tcp   LISTEN 0      128        127.0.0.1:8125       0.0.0.0:*    users:(("netdata",pid=2291,fd=57))

tcp   LISTEN 0      128          0.0.0.0:19999      0.0.0.0:*    users:(("netdata",pid=2291,fd=8))

tcp   LISTEN 0      128            [::1]:8125          [::]:*    users:(("netdata",pid=2291,fd=49))

tcp   LISTEN 0      128             [::]:19999         [::]:*    users:(("netdata",pid=2291,fd=9))

```

🌞 Configurer Netdata pour qu'il vous envoie des alertes
```
[robot-045@web ~]$ sudo nano /etc/netdata/health.d/cpu.conf
[robot-045@web ~]$ cat /etc/netdata/health.d/cpu.conf
alarm: cpu_usage
on: system.cpu
lookup: average -3s percentage foreach user ,system
units: %
every: 10s
warn: $this > 50
crit: $this > 80
info: CPU utilization of users or the system itself.

[robot-045@web ~]$ sudo nano /etc/netdata/health.d/ram-usage.conf
[robot-045@web ~]$ sudo cat /etc/netdata/health.d/ram-usage.conf
 alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 80
  crit: $this > 90
  info: The percentage of RAM being used by the system.

```
// je ne sais pas comment avoir l'api de discorde pour avec des alerte
🌞 Vérifier que les alertes fonctionnent
```
```