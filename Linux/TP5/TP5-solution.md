# TP5 : Self-hosted cloud

# Partie 1 : Mise en place et maîtrise du serveur Web
###  1. Installation
🌞 Installer le serveur Apache
```
[robot-045@web ~]$ sudo dnf install -y httpd
```
🌞 Démarrer le service Apache
```
[robot-045@web ~]$ sudo systemctl start httpd
[robot-045@web ~]$ sudo systemctl status httpd
● httpd.service - The Apache HTTP Server
   Loaded: loaded (/usr/lib/systemd/system/httpd.service; disabled; vendor pre>
   Active: active (running) since Tue 2023-01-17 11:12:24 CET; 31s ago
     Docs: man:httpd.service(8)
 Main PID: 2078 (httpd)
   Status: "Running, listening on: port 80"
    Tasks: 213 (limit: 4916)
   Memory: 29.7M
   CGroup: /system.slice/httpd.service
           ├─2078 /usr/sbin/httpd -DFOREGROUND
           ├─2079 /usr/sbin/httpd -DFOREGROUND
           ├─2080 /usr/sbin/httpd -DFOREGROUND
           ├─2081 /usr/sbin/httpd -DFOREGROUND
           └─2082 /usr/sbin/httpd -DFOREGROUND
[robot-045@web ~]$ sudo ss -alpnt | grep httpd
LISTEN 0      128                *:80              *:*    users:(("httpd",pid=2082,fd=4),("httpd",pid=2081,fd=4),("httpd",pid=2080,fd=4),("httpd",pid=2078,fd=4))
[robot-045@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
```
```
[robot-045@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[robot-045@web ~]$ sudo firewall-cmd --reload
success
[robot-045@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 80/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```
🌞 TEST
```
[robot-045@web ~]$ sudo systemctl is-active httpd
active
[robot-045@web ~]$ sudo systemctl is-enabled httpd
enabled
[robot-045@web ~]$ curl localhost | head
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
$ curl 10.105.1.11 | head
doctype html>
tml>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {

```
### 2. Avancer vers la maîtrise du service
🌞 Le service Apache...
```
[robot-045@web ~]$ cat /usr/lib/systemd/system/httpd.service
# See httpd.service(8) for more information on using the httpd service.

# Modifying this file in-place is not recommended, because changes
# will be overwritten during package upgrades.  To customize the
# behaviour, run "systemctl edit httpd" to create an override unit.

# For example, to pass additional options (such as -D definitions) to
# the httpd binary at startup, create an override unit (as is done by
# systemctl edit) and enter the following:

#       [Service]
#       Environment=OPTIONS=-DMY_DEFINE

[Unit]
Description=The Apache HTTP Server
Wants=httpd-init.service
After=network.target remote-fs.target nss-lookup.target httpd-init.service
Documentation=man:httpd.service(8)

[Service]
Type=notify
Environment=LANG=C

ExecStart=/usr/sbin/httpd $OPTIONS -DFOREGROUND
ExecReload=/usr/sbin/httpd $OPTIONS -k graceful
# Send SIGWINCH for graceful stop
KillSignal=SIGWINCH
KillMode=mixed
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```
🌞 Déterminer sous quel utilisateur tourne le processus Apache
```
[robot-045@web ~]$ cat /etc/httpd/conf/httpd.conf | grep apache
User apache
Group apache
[robot-045@web ~]$ ps -ef | grep apache
apache       802     779  0 15:11 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache       804     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
apache       805     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
apache       806     779  0 15:11 ?        00:00:03 /usr/sbin/httpd -DFOREGROUND
robot-0+    1708    1648  0 15:38 pts/0    00:00:00 grep --color=auto apache
[robot-045@web ~]$ ls -al /usr/share/testpage/
total 12
drwxr-xr-x.  2 root root   24 Jan 17 11:05 .
drwxr-xr-x. 91 root root 4096 Jan 17 11:05 ..
-rw-r--r--.  1 root root 7620 Jul 27 20:04 index.html
```
🌞 Changer l'utilisateur utilisé par Apache
```
[robot-045@web ~]$ sudo useradd robot -d /usr/share/httpd/ -s /sbin/nologin -u 2000
[robot-045@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[robot-045@web ~]$ cat /etc/httpd/conf/httpd.conf | grep robot
User robot
[robot-045@web ~]$ ps -ef | grep robot
root         791       1  0 15:11 ?        00:00:00 login -- robot-045
robot-0+    1601       1  0 15:17 ?        00:00:00 /usr/lib/systemd/systemd --user
robot-0+    1604    1601  0 15:17 ?        00:00:00 (sd-pam)
robot-0+    1611     791  0 15:17 tty1     00:00:00 -bash
root        1643     784  0 15:17 ?        00:00:00 sshd: robot-045 [priv]
robot-0+    1647    1643  0 15:17 ?        00:00:00 sshd: robot-045@pts/0
robot-0+    1648    1647  0 15:17 pts/0    00:00:00 -bash
robot       1761    1758  0 16:02 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
robot       1762    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
robot       1763    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
robot       1764    1758  0 16:02 ?        00:00:01 /usr/sbin/httpd -DFOREGROUND
robot-0+    1994    1648  0 16:09 pts/0    00:00:00 ps -ef
robot-0+    1995    1648  0 16:09 pts/0    00:00:00 grep --color=auto robot
```
🌞 Faites en sorte que Apache tourne sur un autre port
```
[robot-045@web ~]$ sudo nano /etc/httpd/conf/httpd.conf
[robot-045@web ~]$ cat /etc/httpd/conf/httpd.conf | grep 045
Listen 045
[robot-045@web ~]$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
[robot-045@web ~]$ sudo firewall-cmd --add-port=045/tcp --permanent
success
[robot-045@web ~]$ sudo firewall-cmd --reload
success
[robot-045@web ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 45/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[robot-045@web ~]$ sudo systemctl restart httpd
[robot-045@web ~]$ sudo ss -altpn | grep httpd
LISTEN 0      128                *:45              *:*    users:(("httpd",pid=2042,fd=4),("httpd",pid=2041,fd=4),("httpd",pid=2040,fd=4),("httpd",pid=2036,fd=4))
[robot-045@web ~]$ curl 10.105.1.11:045 | head
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  7620  100  7620    0     0   826k      0 --:--:-- --:--:-- --:--:-- 1240k
<!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <title>HTTP Server Test Page powered by: Rocky Linux</title>
    <style type="text/css">
      /*<![CDATA[*/

      html {
```

📁 Fichier /etc/httpd/conf/httpd.conf

# Partie 2 : Mise en place et maîtrise du serveur de base de données

🌞 Install de MariaDB sur db.tp5.linux
```
[robot-045@db ~]$ sudo dnf install mariadb-server
[robot-045@db ~]$ sudo systemctl enable mariadb
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
[robot-045@db ~]$ sudo systemctl start mariadb
[robot-045@db ~]$ sudo mysql_secure_installation
```

🌞 Port utilisé par MariaDB
```
[robot-045@db ~]$ sudo ss -alpnt | grep 80
LISTEN 0      80                 *:3306            *:*    users:(("mysqld",pid=26321,fd=21))
```

```
[robot-045@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[robot-045@db ~]$ sudo firewall-cmd --reload
success
[robot-045@db ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 3306/tcp
  protocols:
  forward: no
  masquerade: no
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

🌞 Processus liés à MariaDB
```
[robot-045@db ~]$ ps -ef | grep mysqld | grep usr
mysql      26321       1  0 09:49 ?        00:00:01 /usr/libexec/mysqld --basedir=/usr
```

# Partie 3 : Configuration et mise en place de NextCloud

## 1.Base de données

🌞 Préparation de la base pour NextCloud
```
[robot-045@db ~]$ sudo mysql -u root -p
[sudo] password for robot-045:
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 16
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'10.105.1.11' IDENTIFIED BY 'pewpewpew';
Query OK, 0 rows affected (0.002 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.105.1.11';
Query OK, 0 rows affected (0.001 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)
```

🌞 Exploration de la base de données
///
```
[robot-045@web ~]$ sudo dnf install mysql -y
```
///
```
[robot-045@web ~]$ mysql -u nextcloud -h 10.105.1.12 -p
Enter password:
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 25
Server version: 5.5.5-10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2022, Oracle and/or its affiliates.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql>
```
```
mysql> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.00 sec)

mysql> USE nextcloud;
Database changed
mysql>  show tables;
Empty set (0.00 sec)
```

🌞 Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données
```
[robot-045@db ~]$ mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 26
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> SELECT User FROM mysql.user;
+-----------+
| User      |
+-----------+
| nextcloud |
| root      |
| root      |
| root      |
+-----------+
4 rows in set (0.001 sec)

MariaDB [(none)]>
```

## 2.Serveur Web et NextCloud
🌞 Install de PHP

🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧

🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧
➜ **Nous pouvez aller voir[Explication](../TP5/Explication.md)**

```
[robot-045@web ~]$ sudo dnf check-update
[robot-045@web ~]$ sudo dnf update
[robot-045@web ~]$ sudo dnf install dnf-utils
```
```
[robot-045@web ~]$ sudo dnf install dnf-utils
[robot-045@web ~]$ sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
[robot-045@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[robot-045@web ~]$ sudo dnf module reset php
[robot-045@web ~]$ sudo dnf module enable php:remi-8.1
[robot-045@web ~]$ sudo dnf install php
[robot-045@web ~]$ php -v
PHP 8.1.14 (cli) (built: Jan  4 2023 06:45:14) (NTS gcc x86_64)
Copyright (c) The PHP Group
Zend Engine v4.1.14, Copyright (c) Zend Technologies
    with Zend OPcache v8.1.14, Copyright (c), by Zend Technologies
```
🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧

🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧🚧

🌞 Install de tous les modules PHP nécessaires pour NextCloud
```
[robot-045@web ~]$ sudo dnf install php-fpm
[robot-045@web ~]$ sudo systemctl enable php-fpm
Created symlink /etc/systemd/system/multi-user.target.wants/php-fpm.service → /usr/lib/systemd/system/php-fpm.service.
[robot-045@web ~]$ sudo systemctl start php-fpm
[robot-045@web ~]$ sudo systemctl status php-fpm
● php-fpm.service - The PHP FastCGI Process Manager
   Loaded: loaded (/usr/lib/systemd/system/php-fpm.service; enabled; vendor preset: disabled)
   Active: active (running) since Tue 2023-01-31 14:24:31 CET; 4s ago
 Main PID: 73494 (php-fpm)
   Status: "Ready to handle connections"
    Tasks: 6 (limit: 4916)
   Memory: 13.1M
   CGroup: /system.slice/php-fpm.service
           ├─73494 php-fpm: master process (/etc/php-fpm.conf)
           ├─73495 php-fpm: pool www
           ├─73496 php-fpm: pool www
           ├─73497 php-fpm: pool www
           ├─73498 php-fpm: pool www
           └─73499 php-fpm: pool www
```
```
[robot-045@web ~]$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
 ...
Installed:
  environment-modules-4.5.2-2.el8.x86_64        fribidi-1.0.4-9.el8.x86_64                    gd3php-2.3.3-8.el8.remi.x86_64
  graphite2-1.3.10-10.el8.x86_64                harfbuzz-1.7.5-3.el8.x86_64                   jbigkit-libs-2.1-14.el8.x86_64
  libXpm-3.5.12-9.el8_7.x86_64                  libaom-3.1.1-1.el8.x86_64                     libavif-0.10.1-3.el8.x86_64
  libdav1d-0.5.2-1.el8.x86_64                   libicu71-71.1-2.el8.remi.x86_64               libimagequant-2.12.5-1.el8.x86_64
  libjpeg-turbo-1.5.3-12.el8.x86_64             libraqm-0.7.0-4.el8.x86_64                    libtiff-4.0.9-26.el8_7.x86_64
  libwebp-1.0.0-5.el8.x86_64                    php81-php-8.1.14-1.el8.remi.x86_64            php81-php-bcmath-8.1.14-1.el8.remi.x86_64
  php81-php-cli-8.1.14-1.el8.remi.x86_64        php81-php-common-8.1.14-1.el8.remi.x86_64     php81-php-fpm-8.1.14-1.el8.remi.x86_64
  php81-php-gd-8.1.14-1.el8.remi.x86_64         php81-php-gmp-8.1.14-1.el8.remi.x86_64        php81-php-intl-8.1.14-1.el8.remi.x86_64
  php81-php-mbstring-8.1.14-1.el8.remi.x86_64   php81-php-mysqlnd-8.1.14-1.el8.remi.x86_64    php81-php-opcache-8.1.14-1.el8.remi.x86_64
  php81-php-pdo-8.1.14-1.el8.remi.x86_64        php81-php-pecl-zip-1.21.1-1.el8.remi.x86_64   php81-php-process-8.1.14-1.el8.remi.x86_64
  php81-php-sodium-8.1.14-1.el8.remi.x86_64     php81-php-xml-8.1.14-1.el8.remi.x86_64        php81-runtime-8.1-2.el8.remi.x86_64
  remi-libzip-1.9.2-3.el8.remi.x86_64           scl-utils-1:2.0.2-15.el8.x86_64               svt-av1-libs-0.8.7-1.el8.x86_64
  tcl-1:8.6.8-2.el8.x86_64

Complete!
```

🌞 Récupérer NextCloud
```
[robot-045@web ~]$ sudo mkdir /var/www/tp5_nextcloud -p
[robot-045@web ~]$ curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output nextcloud.zip
[robot-045@web ~]$ sudo dnf install unzip -y
[robot-045@web ~]$ unzip nextcloud.zip
[robot-045@web ~]$ sudo mv * /var/www/tp5_nextcloud/
```
```
[robot-045@web ~]$ cd /var/www/
[robot-045@web www]$ sudo mkdir test
[robot-045@web www]$ cd test/
[robot-045@web test]$ sudo curl https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip --output -test
[robot-045@web test]$ sudo unzip test  
```
```
[robot-045@web test]$ sudo mv nextcloud /var/www/tp5_nextcloud/
```
```
[robot-045@web ~]$ cd /var/www/tp5_nextcloud/nextcloud/
[robot-045@web nextcloud]$ sudo mv * /var/www/tp5_nextcloud/
[robot-045@web nextcloud]$ sudo mv .htaccess /var/www/tp5_nextcloud/
[robot-045@web nextcloud]$ sudo mv .user.ini /var/www/tp5_nextcloud/
```
```
[robot-045@web ~]$ cd /var/www/
[robot-045@web www]$ sudo chown -R apache:apache tp5_nextcloud/
[robot-045@web www]$ cd tp5_nextcloud/
[robot-045@web tp5_nextcloud]$ ls -al
total 172352
drwxr-xr-x  15 apache apache      4096 Jan 31 15:00 .
drwxr-xr-x.  6 root   root          66 Jan 31 14:47 ..
-rw-rw-r--.  1 apache apache         0 Oct 13 12:44 3
drwxr-xr-x  47 apache apache      4096 Oct  6 14:47 3rdparty
drwxr-xr-x  50 apache apache      4096 Oct  6 14:44 apps
-rw-r--r--   1 apache apache     19327 Oct  6 14:42 AUTHORS
drwxr-xr-x   2 apache apache        67 Oct  6 14:47 config
-rw-r--r--   1 apache apache      4095 Oct  6 14:42 console.php
-rw-r--r--   1 apache apache     34520 Oct  6 14:42 COPYING
drwxr-xr-x  23 apache apache      4096 Oct  6 14:47 core
-rw-r--r--   1 apache apache      6317 Oct  6 14:42 cron.php
drwxr-xr-x   2 apache apache      8192 Oct  6 14:42 dist
-rw-r--r--   1 apache apache      3253 Oct  6 14:42 .htaccess
-rw-r--r--   1 apache apache       156 Oct  6 14:42 index.html
-rw-r--r--   1 apache apache      3456 Oct  6 14:42 index.php
drwxr-xr-x   6 apache apache       125 Oct  6 14:42 lib
drwxr-xr-x   2 apache apache         6 Jan 31 15:00 nextcloud
-rw-rw-r--   1 apache apache 176341139 Jan 31 14:42 nextcloud.zip
-rw-r--r--   1 apache apache       283 Oct  6 14:42 occ
drwxr-xr-x   2 apache apache        23 Oct  6 14:42 ocm-provider
drwxr-xr-x   2 apache apache        55 Oct  6 14:42 ocs
drwxr-xr-x   2 apache apache        23 Oct  6 14:42 ocs-provider
-rw-r--r--   1 apache apache      3139 Oct  6 14:42 public.php
-rw-r--r--   1 apache apache      5426 Oct  6 14:42 remote.php
drwxr-xr-x   4 apache apache       133 Oct  6 14:42 resources
-rw-r--r--   1 apache apache        26 Oct  6 14:42 robots.txt
-rw-r--r--   1 apache apache      2452 Oct  6 14:42 status.php
drwxr-xr-x   3 apache apache        35 Oct  6 14:42 themes
drwxr-xr-x   2 apache apache        43 Oct  6 14:44 updater
-rw-r--r--   1 apache apache       101 Oct  6 14:42 .user.ini
-rw-r--r--   1 apache apache       387 Oct  6 14:47 version.php
```

🌞 Adapter la configuration d'Apache
```
[robot-045@web ~]$ cd /etc/httpd/conf.d
[robot-045@web conf.d]$ sudo nano tp5_conf.conf
[robot-045@web conf.d]$ cat /etc/httpd/conf.d/tp5_conf.conf
<VirtualHost *:80>
  # on indique le chemin de notre webroot
  DocumentRoot /var/www/tp5_nextcloud/
  # on précise le nom que saisissent les clients pour accéder au service
  ServerName  web.tp5.linux

  # on définit des règles d'accès sur notre webroot
  <Directory /var/www/tp5_nextcloud/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews
    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
```

🌞 Redémarrer le service Apache 
```
[robot-045@web conf.d]$ sudo systemctl restart httpd
```

## 3.Finaliser l'installation de NextCloud

🌞 Exploration de la base de données
```
[robot-045@db ~]$ sudo mysql -u root -p
Enter password:
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 8
Server version: 10.3.35-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> select count(*) as NumberOfTableInNextCloud from INFORMATION_SCHEMA.TABLES where TABLE_SCHEMA = 'ne
    '> xtcloud';
+--------------------------+
| NumberOfTableInNextCloud |
+--------------------------+
|                        0 |
+--------------------------+
1 row in set (0.004 sec)

MariaDB [(none)]> select count(*) from information_schema.tables where table_type = 'BASE TABLE';
+----------+
| count(*) |
+----------+
|       83 |
+----------+
1 row in set (0.029 sec)
```

