# Explication de mon travail qui est différent

**Étape 1. La première étape consiste à mettre à jour le système avec la dernière version. Pour ce faire, exécutez les commandes suivantes :**

```bash
sudo dnf check-update
sudo dnf update
sudo dnf install dnf-utils
```

**Installez et activez le référentiel EPEL et REMI.**
    REMI et le référentiel Extra Packages for Enterprise Linux (EPEL)
    
----- 

## Qu'est-ce que l'EPEL

**EPEL ( Extra Packages for Enterprise Linux ) est un projet de référentiel open source et gratuit basé sur la communauté de l' équipe Fedora qui fournit des packages logiciels complémentaires 100% de haute qualité pour la distribution Linux, y compris RHEL ( Red Hat Enterprise Linux ), CentOS Stream , AlmaLinux et Rocky Linux .**

----- 

```bash
# On ajoute le dépôt EPEL
sudo dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
# On ajoute le dépôt REMI
sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
```

**Étape 3. Installation de PHP sur Rocky Linux 8.**
    Par défaut, PHP est disponible sur le référentiel de base Rocky Linux 8. 
    La commande suivante ci-dessous pour réinitialiser le module PHP par défaut et activer le module PHP 8 à l'aide de la commande suivante :
```bash
sudo dnf module reset php
sudo dnf module enable php:remi-8.1
```

 -- J'ai installer celle la pour savoir si sa fonctionne puis j'ai installer celle que tu donne dans le tp 

**Étape 4. Installation des extensions PHP.**
```bash
sudo dnf install php-extension-name
```

**Par exemple, installez le module PHP-fpm pour Nginx :**
```bash
sudo dnf installer php - fpm
```
    Une fois l'installation terminée.
    Il faut démarrez le service PHP-fpm et l'activez pour qu'il s'exécute automatiquement à chaque démarrage à l'aide de la commande suivante :
```bash
sudo systemctl enable php - fpm 
sudo systemctl start php - fpm 
sudo systemctl status php - fpm
```

# Comparaison avec ton travaille  

**Install de PHP**

```bash
# On ajoute le dépôt CRB
$ sudo dnf config-manager --set-enabled crb
# On ajoute le dépôt REMI
$ sudo dnf install dnf-utils http://rpms.remirepo.net/enterprise/remi-release-9.rpm -y

# On liste les versions de PHP dispos, au passage on va pouvoir accepter les clés du dépôt REMI
$ dnf module list php

# On active le dépôt REMI pour récupérer une version spécifique de PHP, celle recommandée par la doc de NextCloud
$ sudo dnf module enable php:remi-8.1 -y

# Eeeet enfin, on installe la bonne version de PHP : 8.1
$ sudo dnf install -y php81-php
```

**Install de tous les modules PHP nécessaires pour NextCloud**

```bash
# eeeeet euuuh boom. Là non plus j'ai pas pondu ça, c'est la doc :
$ sudo dnf install -y libxml2 openssl php81-php php81-php-ctype php81-php-curl php81-php-gd php81-php-iconv php81-php-json php81-php-libxml php81-php-mbstring php81-php-openssl php81-php-posix php81-php-session php81-php-xml php81-php-zip php81-php-zlib php81-php-pdo php81-php-mysqlnd php81-php-intl php81-php-bcmath php81-php-gmp
```