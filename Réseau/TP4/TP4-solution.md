# TP4 : TCP, UDP et services réseau

# I. First steps

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**

Fusion 360 :

- IP dst = 15.197.213.252
- Port dst = :443
- Port src = :52831
```
PS C:\Windows\system32> netstat -b -n

[Fusion360.exe]
  TCP    10.33.16.166:52831     15.197.213.252:443      TIME_WAIT
```
Steam :

- IP dst = 95.100.86.63
- Port dst = :443
- Port src = :61357 
```
PS C:\Windows\system32> netstat -b -n

[steamwebhelper.exe]
  TCP    10.33.16.166:61357     95.100.86.63:443    TIME_WAIT
```
Discord :

- IP dst = 162.159.137.232
- Port dst = :443
- Port src = :61311 
```
PS C:\Windows\system32> netstat -b -n

[Discord.exe]
  TCP    10.33.16.166:61311     162.159.137.232:443      CLOSE_WAIT
```
Teams :

- IP dst = 104.46.162.226
- Port dst = :443
- Port src = :61431 
```
PS C:\Windows\system32> netstat -b -n

[Teams.exe]
  TCP    10.33.16.166:61431     104.46.162.226:443     ESTABLISHED
```

🦈🦈🦈🦈🦈  Une capture pour chaque application, qui met bien en évidence le trafic en question.

# II. Mise en place

## 1. SSH

🖥️ **Machine `node1.tp4.b1`**

🌞 **Examinez le trafic dans Wireshark**

```
tp4_ssh.pcapng  🦈 
```

🌞 **Demandez aux OS**

- repérez, avec une commande adaptée (`netstat` ou `ss`), la connexion SSH depuis votre machine
```
PS C:\Windows\system32> netstat -p TCP -n -b
Connexions actives
  Proto  Adresse locale         Adresse distante       État
  TCP    10.4.1.1:63264         10.4.1.11:22           ESTABLISHED
 [ssh.exe]
```
- ET repérez la connexion SSH depuis votre VM
```
[robot-045@node1 ~]$ ss
Netid              State               Recv-Q               Send-Q                                           Local Address:Port                              Peer Address:Port               Process
tcp                ESTAB               0                    0                                                    10.4.1.11:ssh                                   10.4.1.1:63264
```

## 2. Routage

🖥️ **Machine `router.tp4.b1`**

# III. DNS

## 2. Setup

🖥️ **Machine `dns-server.tp4.b1`**

🌞 **Dans le rendu, je veux**

- un `cat` des fichiers de conf

**Fichier de conf principal**

**Et pour les fichiers de zone**

- un `systemctl status named` qui prouve que le service tourne bien
- une commande `ss` qui prouve que le service écoute bien sur un port

🌞 **Ouvrez le bon port dans le firewall**

- grâce à la commande `ss` vous devrez avoir repéré sur quel port tourne le service
- ouvrez ce port dans le firewall de la machine `dns-server.tp4.b1` (voir le mémo réseau Rocky)

## 3. Test

🌞 **Sur la machine `node1.tp4.b1`**

- configurez la machine pour qu'elle utilise votre serveur DNS quand elle a besoin de résoudre des noms
- assurez vous que vous pouvez :
  - résoudre des noms comme `node1.tp4.b1` et `dns-server.tp4.b1`
  - mais aussi des noms comme `www.google.com`

🌞 **Sur votre PC**

- utilisez une commande pour résoudre le nom `node1.tp4.b1` en utilisant `10.4.1.201` comme serveur DNS

🦈 **Capture d'une requête DNS vers le nom `node1.tp4.b1` ainsi que la réponse**