# TP3 : On va router des trucs
-------------------
Au menu de ce TP, on va revoir un peu ARP et IP histoire de **se mettre en jambes dans un environnement avec des VMs**.
Puis on mettra en place **un routage simple, pour permettre à deux LANs de communiquer**.
------------------
## I. ARP
### 1. Echange ARP

🌞**Générer des requêtes ARP**

- effectuer un `ping` d'une machine à l'autre
- observer les tables ARP des deux machines
- repérer l'adresse MAC de `john` dans la table ARP de `marcel` et vice-versa
- prouvez que l'info est correcte (que l'adresse MAC que vous voyez dans la table est bien celle de la machine correspondante)
  - une commande pour voir la MAC de `marcel` dans la table ARP de `john`
  - et une commande pour afficher la MAC de `marcel`, depuis `marcel`
```
john
[robot-045@localhost ~]$ ping 10.3.1.12
PING 10.3.1.12 (10.3.1.12) 56(84) bytes of data.
64 bytes from 10.3.1.12: icmp_seq=1 ttl=64 time=2.48 ms
64 bytes from 10.3.1.12: icmp_seq=2 ttl=64 time=1.85 ms
64 bytes from 10.3.1.12: icmp_seq=3 ttl=64 time=4.25 ms
64 bytes from 10.3.1.12: icmp_seq=4 ttl=64 time=2.45 ms
^C
--- 10.3.1.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3009ms
rtt min/avg/max/mdev = 1.847/2.758/4.253/0.901 ms
-------------------------------------------------------------
[robot-045@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:33 REACHABLE
10.3.1.12 dev enp0s8 lladdr 08:00:27:ea:31:dc REACHABLE <----Mac
-------------------------------------------------------------
[robot-045@localhost ~]$ ip neigh show 10.3.1.12
10.3.1.12 dev enp0s8 lladdr 08:00:27:ea:31:dc STALE
```

```
marcel
[robot-045@localhost ~]$ ping 10.3.1.11
PING 10.3.1.11 (10.3.1.11) 56(84) bytes of data.
64 bytes from 10.3.1.11: icmp_seq=1 ttl=64 time=1.35 ms
64 bytes from 10.3.1.11: icmp_seq=2 ttl=64 time=0.558 ms
64 bytes from 10.3.1.11: icmp_seq=3 ttl=64 time=1.04 ms
64 bytes from 10.3.1.11: icmp_seq=4 ttl=64 time=2.46 ms
^C
--- 10.3.1.11 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3051ms
rtt min/avg/max/mdev = 0.558/1.349/2.458/0.699 ms
-------------------------------------------------------------
[robot-045@localhost ~]$ ip neigh show
10.3.1.11 dev enp0s8 lladdr 08:00:27:48:b8:71 REACHABLE <---MAc
10.3.1.1 dev enp0s8 lladdr 0a:00:27:00:00:33 REACHABLE
-------------------------------------------------------------
[robot-045@localhost ~]$ ip a
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:ea:31:dc brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.12/24 brd 10.3.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feea:31dc/64 scope link
       valid_lft forever preferred_lft forever
```
### 2. Analyse de trames

🌞**Analyse de trames**

- utilisez la commande `tcpdump` pour réaliser une capture de trame
- videz vos tables ARP, sur les deux machines, puis effectuez un `ping`
```
marcel
[robot-045@localhost ~]$ sudo tcpdump -i enp0s8 -c 10
dropped privs to tcpdump
tcpdump: verbose output suppressed, use -v or -vv for full protocol decode
listening on enp0s8, link-type EN10MB (Ethernet), capture size 262144 bytes
15:51:11.227659 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 2198492526:2198492586, ack 2642796424, win 295, length 60
15:51:11.228067 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 60:248, ack 1, win 295, length 188
15:51:11.228309 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 248, win 8191, length 0
15:51:11.228977 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 248:644, ack 1, win 295, length 396
15:51:11.229686 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 644:800, ack 1, win 295, length 156
15:51:11.230066 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 800, win 8195, length 0
15:51:11.230268 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 800:1060, ack 1, win 295, length 260
15:51:11.230943 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 1060:1216, ack 1, win 295, length 156
15:51:11.231232 IP 10.3.1.1.55794 > localhost.localdomain.ssh: Flags [.], ack 1216, win 8193, length 0
15:51:11.231415 IP localhost.localdomain.ssh > 10.3.1.1.55794: Flags [P.], seq 1216:1372, ack 1, win 295, length 156
10 packets captured
11 packets received by filter
0 packets dropped by kernel
```
🦈 **Capture réseau `tp3_arp.pcapng`** qui contient un ARP request et un ARP reply

-----------------------
## II. Routage
### 1. Mise en place du routage

🌞**Activer le routage sur le noeud `router`**
```
[robot-045@localhost ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  forward: no
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
[robot-045@localhost ~]$ sudo firewall-cmd --get-active-zone
public
  interfaces: enp0s8 enp0s9
[robot-045@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[robot-045@localhost ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```
🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**

- il faut taper une commande `ip route add` pour cela, voir mémo
- il faut ajouter une seule route des deux côtés
- une fois les routes en place, vérifiez avec un `ping` que les deux machines peuvent se joindre
```
john :

[robot-045@localhost ~]$ ip route show
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s8 proto static metric 100

[robot-045@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.90 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.93 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=3.23 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=5.16 ms
^C
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 1.900/3.053/5.161/1.329 ms
```
```
marcel :

[robot-045@localhost ~]$ ip route show
10.3.1.11 via 10.3.2.254 dev enp0s8 proto static metric 100
10.3.2.0/24 dev enp0s8 proto kernel scope link src 10.3.2.12 metric 100

[robot-045@localhost ~]$ ip route show
10.3.1.11 via 10.3.2.254 dev enp0s8 proto static metric 100
10.3.2.0/24 dev enp0s8 proto kernel scope link src 10.3.2.12 metric 100
[robot-045@localhost ~]$ ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=64 time=0.042 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=64 time=0.102 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=64 time=0.147 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=64 time=0.059 ms
^C
--- 10.3.2.12 ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3108ms
rtt min/avg/max/mdev = 0.042/0.087/0.147/0.041 ms
```
### 2. Analyse de trames

🌞**Analyse des échanges ARP**

- videz les tables ARP des trois noeuds
- effectuez un `ping` de `john` vers `marcel`
- regardez les tables ARP des trois noeuds
- essayez de déduire un peu les échanges ARP qui ont eu lieu
- répétez l'opération précédente (vider les tables, puis `ping`), en lançant `tcpdump` sur `marcel`
- **écrivez, dans l'ordre, les échanges ARP qui ont eu lieu, puis le ping et le pong, je veux TOUTES les trames** utiles pour l'échange

Par exemple (copiez-collez ce tableau ce sera le plus simple) :

| ordre | type trame  | IP source | MAC source              | IP destination | MAC destination            |
|-------|-------------|-----------|-------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `john` `08:00:27:a4:21:98` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | `router` `08:00:27:2f:71:d6`                       | x              | `john` `08:00:27:a4:21:98`    |
| ...   | ...         | ...       | ...                     |                |                            |
| ?     | Ping        | `john` `10.3.1.11`         | `john` `08:00:27:a4:21:98`                       | `router``10.3.1.254`              | `router` `08:00:27:2f:71:d6`                          |
| ?     | Pong        | `router``10.3.1.254`          | `router` `08:00:27:2f:71:d6`                       | `john` `10.3.1.11`              | `john` `08:00:27:a4:21:98`                          |

```
```
🦈 **Capture réseau `tp3_routage_marcel.pcapng`**

### 3. Accès internet

🌞**Donnez un accès internet à vos machines**

- ajoutez une carte NAT en 3ème inteface sur le `router` pour qu'il ait un accès internet
- ajoutez une route par défaut à `john` et `marcel`
```
john

sudo ip route add default via 10.3.1.254 dev enp0s8

[robot-045@localhost ~]$ ip route
default via 10.3.1.254 dev enp0s8 <------
10.3.1.0/24 dev enp0s8 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s8 proto static metric 100
```
```
marcel

sudo ip route add default via 10.3.2.254 dev enp0s8

[robot-045@localhost ~]$ ip route
default via 10.3.2.254 dev enp0s8 <------
10.3.1.11 via 10.3.2.254 dev enp0s8 proto static metric 100
10.3.2.0/24 dev enp0s8 proto kernel scope link src 10.3.2.12 metric 100
```
  - vérifiez que vous avez accès internet avec un `ping`
  - le `ping` doit être vers une IP, PAS un nom de domaine
  ```
[robot-045@localhost ~]$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=62 time=5.74 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=62 time=7.67 ms
^C
--- 1.1.1.1 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 5.735/6.700/7.665/0.965 ms
```
- donnez leur aussi l'adresse d'un serveur DNS qu'ils peuvent utiliser
  - vérifiez que vous avez une résolution de noms qui fonctionne avec `dig`
 ```
 [robot-045@localhost ~]$ dig gitlab.com

; <<>> DiG 9.11.36-RedHat-9.11.36-3.el8_6.1 <<>> gitlab.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 64625
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;gitlab.com.                    IN      A

;; ANSWER SECTION:
gitlab.com.             248     IN      A       172.65.251.78

;; Query time: 30 msec
;; SERVER: 1.1.1.1#53(1.1.1.1)
;; WHEN: Tue Oct 25 11:03:04 CEST 2022
;; MSG SIZE  rcvd: 55
 ```
  - puis avec un `ping` vers un nom de domaine
```
[robot-045@localhost ~]$ ping gitlab.com
PING gitlab.com (172.65.251.78) 56(84) bytes of data.
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=1 ttl=53 time=26.6 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=2 ttl=53 time=30.2 ms
64 bytes from 172.65.251.78 (172.65.251.78): icmp_seq=3 ttl=53 time=30.4 ms
^C
--- gitlab.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 26.579/29.046/30.367/1.751 ms
```
🌞**Analyse de trames**

- effectuez un `ping 8.8.8.8` depuis `john`
- capturez le ping depuis `john` avec `tcpdump`
- analysez un ping aller et le retour qui correspond et mettez dans un tableau :

| ordre | type trame | IP source          | MAC source              | IP destination | MAC destination |     |
|-------|------------|--------------------|-------------------------|----------------|-----------------|-----|
| 1     | ping       | `john` `10.3.1.11` | `john` `08:00:27:a4:21:98` | `8.8.8.8`      | `08:00:27:9a:dd:5a`               |     |
| 2     | pong       | `8.8.8.8`               | `08:00:27:9a:dd:5a`                    | `john` `10.3.1.11`            | `john` `08:00:27:a4:21:98`            | ... |

🦈 **Capture réseau `tp3_routage_internet.pcapng`**

--------------------
## III. DHCP
### 1. Mise en place du serveur DHCP

🌞**Sur la machine `john`, vous installerez et configurerez un serveur DHCP** (go Google "rocky linux dhcp server").

- installation du serveur sur `john`
- créer une machine `bob`
- faites lui récupérer une IP en DHCP à l'aide de votre serveur
```
```
🌞**Améliorer la configuration du DHCP**

- ajoutez de la configuration à votre DHCP pour qu'il donne aux clients, en plus de leur IP :
  - une route par défaut
  - un serveur DNS à utiliser
- récupérez de nouveau une IP en DHCP sur `bob` pour tester :
  - `marcel` doit avoir une IP
    - vérifier avec une commande qu'il a récupéré son IP
    - vérifier qu'il peut `ping` sa passerelle
  - il doit avoir une route par défaut
    - vérifier la présence de la route avec une commande
    - vérifier que la route fonctionne avec un `ping` vers une IP
  - il doit connaître l'adresse d'un serveur DNS pour avoir de la résolution de noms
    - vérifier avec la commande `dig` que ça fonctionne
    - vérifier un `ping` vers un nom de domaine
```
```
### 2. Analyse de trames

🌞**Analyse de trames**

- lancer une capture à l'aide de `tcpdump` afin de capturer un échange DHCP
- demander une nouvelle IP afin de générer un échange DHCP
- exportez le fichier `.pcapng`
```
```
🦈 **Capture réseau `tp3_dhcp.pcapng`**
