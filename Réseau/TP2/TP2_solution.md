# TP2 : Ethernet, IP, et ARP
------------------------------
# 0. Prérequis

**Pour Wireshark, c'est pareil,** NO SCREENS. La marche à suivre :

- vous capturez le trafic que vous avez à capturer
- vous stoppez la capture (bouton carré rouge en haut à gauche)
- vous sélectionnez les paquets/trames intéressants (CTRL + clic)
- File > Export Specified Packets...
- dans le menu qui s'ouvre, cochez en bas "Selected packets only"
- sauvegardez, ça produit un fichier `.pcapng` (qu'on appelle communément "un ptit PCAP frer") que vous livrerez dans le dépôt git

**Si vous voyez le p'tit pote 🦈 c'est qu'il y a un PCAP à produire et à mettre dans votre dépôt git de rendu.** 

-------------------------------
# I. Setup IP

**Mettez en place une configuration réseau fonctionnelle entre les deux machines**

- vous renseignerez dans le compte rendu :
  - les deux IPs choisies, en précisant le masque
  - l'adresse de réseau
  - l'adresse de broadcast
- vous renseignerez aussi les commandes utilisées pour définir les adresses IP *via* la ligne de commande
```
PS C:\Users\lanfr> netsh interface ipv4 set address name="Ethernet" static 10.25.24.2 255.255.255.0
L'opération demandée requiert une élévation (Exécuter en tant qu'administrateur).
```
**Prouvez que la connexion est fonctionnelle entre les deux machines**

- un `ping` suffit !
```
PS C:\Users\lanfr> ping 10.25.24.1

Envoi d’une requête 'Ping'  10.25.24.1 avec 32 octets de données :
Réponse de 10.25.24.1 : octets=32 temps=1 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=1 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=1 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=2 ms TTL=128

Statistiques Ping pour 10.25.24.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
```
**Wireshark it**

- `ping` ça envoie des paquets de type ICMP (c'est pas de l'IP, c'est un de ses frères)
  - les paquets ICMP sont encapsulés dans des trames Ethernet, comme les paquets IP
  - il existe plusieurs types de paquets ICMP, qui servent à faire des trucs différents
- **déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par `ping`**
  - pour le ping que vous envoyez
  - et le pong que vous recevez en retour
```
ping
```
🦈 **PCAP qui contient les paquets ICMP qui vous ont permis d'identifier les types ICMP**

-------------------------
# II. ARP my bro
**Check the ARP table**

- utilisez une commande pour afficher votre table ARP
- déterminez la MAC de votre binome depuis votre table ARP
- déterminez la MAC de la *gateway* de votre réseau
  - celle de votre réseau physique, WiFi, genre YNOV, car il n'y en a pas dans votre ptit LAN
  - c'est juste pour vous faire manipuler un peu encore :)
```
PS C:\Users\lanfr> arp -a

Interface : 10.25.24.2 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.25.24.1            c8-5a-cf-b7-b4-da     dynamique
  10.25.27.255          ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
**Manipuler la table ARP**

- utilisez une commande pour vider votre table ARP
- prouvez que ça fonctionne en l'affichant et en constatant les changements
- ré-effectuez des pings, et constatez la ré-apparition des données dans la table ARP
```
PS C:\Windows\system32> netsh interface IP delete arpcache
Ok.

PS C:\Windows\system32> arp -a

Interface : 10.25.24.2 --- 0x7
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
```
```
PS C:\Windows\system32> ping 10.25.24.1

Envoi d’une requête 'Ping'  10.25.24.1 avec 32 octets de données :
Réponse de 10.25.24.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=2 ms TTL=128
Réponse de 10.25.24.1 : octets=32 temps=3 ms TTL=128

Statistiques Ping pour 10.25.24.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 2ms, Maximum = 3ms, Moyenne = 2ms
PS C:\Windows\system32> arp -a

Interface : 10.25.24.2 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.25.24.1            c8-5a-cf-b7-b4-da     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
```
**Wireshark it**

- vous savez maintenant comment forcer un échange ARP : il sufit de vider la table ARP et tenter de contacter quelqu'un, l'échange ARP se fait automatiquement
- mettez en évidence les deux trames ARP échangées lorsque vous essayez de contacter quelqu'un pour la "première" fois
  - déterminez, pour les deux trames, les adresses source et destination
  - déterminez à quoi correspond chacune de ces adresses
```
ARP
```
🦈 **PCAP qui contient les trames ARP***

---------------------------------------------------
# II.5 Interlude hackerzz

**Chose promise chose due, on va voir les bases de l'usurpation d'identité en réseau : on va parler d'*ARP poisoning*.**

Le principe est simple : on va "empoisonner" la table ARP de quelqu'un d'autre.  
Plus concrètement, on va essayer d'introduire des fausses informations dans la table ARP de quelqu'un d'autre.

Entre introduire des fausses infos et usurper l'identité de quelqu'un il n'y a qu'un pas hihi.


➜ **Le principe de l'attaque**

- on admet Alice, Bob et Eve, tous dans un LAN, chacun leur PC
- leur configuration IP est ok, tout va bien dans le meilleur des mondes
- **Eve 'lé pa jonti** *(ou juste un agent de la CIA)* : elle aimerait s'immiscer dans les conversations de Alice et Bob
  - pour ce faire, Eve va empoisonner la table ARP de Bob, pour se faire passer pour Alice
  - elle va aussi empoisonner la table ARP d'Alice, pour se faire passer pour Bob
  - ainsi, tous les messages que s'envoient Alice et Bob seront en réalité envoyés à Eve

➜ **La place de ARP dans tout ça**

- ARP est un principe de question -> réponse (broadcast -> *reply*)
- IL SE TROUVE qu'on peut envoyer des *reply* à quelqu'un qui n'a rien demandé :)
- il faut donc simplement envoyer :
  - une trame ARP reply à Alice qui dit "l'IP de Bob se trouve à la MAC de Eve" (IP B -> MAC E)
  - une trame ARP reply à Bob qui dit "l'IP de Alice se trouve à la MAC de Eve" (IP A -> MAC E)
- ha ouais, et pour être sûr que ça reste en place, il faut SPAM sa mum, genre 1 reply chacun toutes les secondes ou truc du genre
  - bah ui ! Sinon on risque que la table ARP d'Alice ou Bob se vide naturellement, et que l'échange ARP normal survienne
  - aussi, c'est un truc possible, mais pas normal dans cette utilisation là, donc des fois bon, ça chie, DONC ON SPAM




➜ J'peux vous aider à le mettre en place, mais **vous le faites uniquement dans un cadre privé, chez vous, ou avec des VMs**

➜ **Je vous conseille 3 machines Linux**, Alice Bob et Eve. La commande 

--------------------------------------
# III. DHCP you too my brooo

**Wireshark it**

- identifiez les 4 trames DHCP lors d'un échange DHCP
  - mettez en évidence les adresses source et destination de chaque trame
- identifiez dans ces 4 trames les informations **1**, **2** et **3** dont on a parlé juste au dessus
```
DHCP

PS C:\Windows\system32> netsh interface ipv4 set address name="Wi-Fi" static 222.222.222.222

PS C:\Windows\system32> netsh interface ip set address "Wi-Fi" dhcp

```
🦈 **PCAP qui contient l'échange DORA**