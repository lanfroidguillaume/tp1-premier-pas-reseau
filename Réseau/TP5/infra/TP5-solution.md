# Sujet Réseau et Infra

# I. Dumb switch
## 2. Adressage topologie 1

| Node  | IP            |
|-------|---------------|
| `pc1` | `10.5.1.1/24` |
| `pc2` | `10.5.1.2/24` |

## 3. Setup topologie 1

🌞 **Commençons simple**

- définissez les IPs statiques sur les deux VPCS
```
PC1> ip 10.5.1.1/24
Checking for duplicate address...
PC1 : 10.5.1.1 255.255.255.0

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.5.1.1/24
GATEWAY     : 0.0.0.0
DNS         :
```
```
PC2> ip 10.5.1.2/24
Checking for duplicate address...
PC2 : 10.5.1.2 255.255.255.0

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.5.1.2/24
GATEWAY     : 0.0.0.0
DNS         :
```
- `ping` un VPCS depuis l'autre
```
PC1> ping 10.5.1.2

84 bytes from 10.5.1.2 icmp_seq=1 ttl=64 time=3.756 ms
84 bytes from 10.5.1.2 icmp_seq=2 ttl=64 time=9.913 ms
84 bytes from 10.5.1.2 icmp_seq=3 ttl=64 time=6.881 ms
84 bytes from 10.5.1.2 icmp_seq=4 ttl=64 time=4.258 ms

```
```
PC2> ping 10.5.1.1/24

84 bytes from 10.5.1.1 icmp_seq=1 ttl=64 time=2.156 ms
84 bytes from 10.5.1.1 icmp_seq=2 ttl=64 time=1.988 ms
84 bytes from 10.5.1.1 icmp_seq=3 ttl=64 time=5.787 ms
84 bytes from 10.5.1.1 icmp_seq=4 ttl=64 time=2.625 ms

```

# II. VLAN
## 2. Adressage topologie 2

| Node  | IP             | VLAN |
|-------|----------------|------|
| `pc1` | `10.5.10.1/24` | 10   |
| `pc2` | `10.5.10.2/24` | 10   |
| `pc3` | `10.5.10.3/24` | 20   |

### 3. Setup topologie 2

🌞 **Adressage**

- définissez les IPs statiques sur tous les VPCS
```
PC1> ip 10.5.10.1/24
Checking for duplicate address...
PC1 : 10.5.10.1 255.255.255.0

PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.5.10.1/24
GATEWAY     : 0.0.0.0
DNS         :
```
```
PC2> ip 10.5.10.2/24
Checking for duplicate address...
PC2 : 10.5.10.2 255.255.255.0

PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.5.10.2/24
GATEWAY     : 0.0.0.0
DNS         :
```
```
PC3> ip 10.5.10.3/24
Checking for duplicate address...
PC3 : 10.5.10.3 255.255.255.0

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.5.10.3/24
GATEWAY     : 0.0.0.0
DNS         :
```
- vérifiez avec des `ping` que tout le monde se ping

//PC1
```
PC1> ping 10.5.10.2/24

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=1.381 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=1.013 ms
84 bytes from 10.5.10.2 icmp_seq=3 ttl=64 time=0.550 ms
```
```
PC1> ping 10.5.10.3/24

84 bytes from 10.5.10.3 icmp_seq=1 ttl=64 time=0.195 ms
84 bytes from 10.5.10.3 icmp_seq=2 ttl=64 time=0.280 ms
```
//PC2
```
PC2> ping 10.5.10.1/24

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=1.451 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=0.713 ms
84 bytes from 10.5.10.1 icmp_seq=3 ttl=64 time=0.569 ms
```
```
PC2> ping 10.5.10.3/24

84 bytes from 10.5.10.3 icmp_seq=1 ttl=64 time=2.044 ms
84 bytes from 10.5.10.3 icmp_seq=2 ttl=64 time=1.134 ms
84 bytes from 10.5.10.3 icmp_seq=3 ttl=64 time=0.788 ms
```
//PC3
```
PC3> ping 10.5.10.1/24

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=1.698 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=0.852 ms
84 bytes from 10.5.10.1 icmp_seq=3 ttl=64 time=1.335 ms
```
```
PC3> ping 10.5.10.2/24

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=1.506 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=1.315 ms
84 bytes from 10.5.10.2 icmp_seq=3 ttl=64 time=0.950 ms
```

🌞 **Configuration des VLANs**

- référez-vous [à la section VLAN du mémo Cisco](../../cours/memo/memo_cisco.md#8-vlan)
- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN (voir [le tableau d'adressage de la topo 2 juste au dessus](#2-adressage-topologie-2))
  - ici, tous les ports sont en mode *access* : ils pointent vers des clients du réseau

🌞 **Vérif**

- `pc1` et `pc2` doivent toujours pouvoir se ping
- `pc3` ne ping plus personne

# III. Routing
## 2. Adressage topologie 3

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `clients` | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`        | `admins`         | `servers`        |
|--------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`  | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`  | `10.5.10.2/24`   | x                | x                |
| `adm1.admins.tp5`  | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5` | x                | x                | `10.5.30.1/24`   |
| `r1`               | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 3

🖥️ VM `web1.servers.tp5`, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞 **Adressage**

- définissez les IPs statiques sur toutes les machines **sauf le *routeur***

```
PC1> show ip

NAME        : PC1[1]
IP/MASK     : 10.5.10.1/24
GATEWAY     : 10.5.10.254
DNS         :
```
```
PC2> show ip

NAME        : PC2[1]
IP/MASK     : 10.5.10.2/24
GATEWAY     : 10.5.10.254
DNS         :
```
```
PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.5.20.1/24
GATEWAY     : 10.5.20.254
DNS         :
```

🌞 **Configuration des VLANs**

- déclaration des VLANs sur le switch `sw1`
- ajout des ports du switches dans le bon VLAN 

```
IOU1#show vlan br

VLAN Name                             Status    Ports
---- -------------------------------- --------- -------------------------------
1    default                          active    Et1/1, Et1/2, Et1/3, Et2/0
                                                Et2/1, Et2/2, Et2/3, Et3/0
                                                Et3/1, Et3/2, Et3/3
10   clients                          active    Et0/1, Et0/2
20   admins                           active    Et0/3
30   servers                          active    Et1/0
1002 fddi-default                     act/unsup
1003 token-ring-default               act/unsup
1004 fddinet-default                  act/unsup
1005 trnet-default                    act/unsup
```
- il faudra ajouter le port qui pointe vers le *routeur* comme un *trunk* : c'est un port entre deux équipements réseau 

```
IOU1#show interface trunk

Port        Mode             Encapsulation  Status        Native vlan
Et0/0       on               802.1q         trunking      1

Port        Vlans allowed on trunk
Et0/0       1-4094

Port        Vlans allowed and active in management domain
Et0/0       1,10,20,30

Port        Vlans in spanning tree forwarding state and not pruned
Et0/0       1,10,20,30
```


🌞 **Config du *routeur***

- attribuez ses IPs au *routeur*

```
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#int fa 0/0
R1(config-if)#no shutdown
R1(config-if)#int fa 0/0.10
R1(config-subif)#encapsulation dot1Q 10
R1(config-subif)#ip address 10.5.10.254 255.255.255.0
R1(config)#int fa 0/0.20
R1(config-subif)#encapsulation dot1Q 20
R1(config-subif)#ip address 10.5.20.254 255.255.255.0
R1(config-subif)#int fa 0/0.30
R1(config-subif)#encapsulation dot1Q 30
R1(config-subif)#ip address 10.5.30.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
```

🌞 **Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau
- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux
  - ajoutez une route par défaut sur les VPCS
  - ajoutez une route par défaut sur la machine virtuelle
  - testez des `ping` entre les réseaux

//PC1
```
PC1> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=64 time=6.368 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=64 time=3.495 ms
84 bytes from 10.5.10.2 icmp_seq=3 ttl=64 time=5.842 ms
```
```
PC1> ping 10.5.20.1

84 bytes from 10.5.20.1 icmp_seq=1 ttl=63 time=57.403 ms
84 bytes from 10.5.20.1 icmp_seq=2 ttl=63 time=53.314 ms
84 bytes from 10.5.20.1 icmp_seq=3 ttl=63 time=15.578 ms
```
```

```
//PC2
```
PC2> ping 10.5.10.1

84 bytes from 10.5.10.1 icmp_seq=1 ttl=64 time=1.974 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=64 time=2.944 ms
84 bytes from 10.5.10.1 icmp_seq=3 ttl=64 time=1.430 ms
```
```
PC2> ping 10.5.20.1

84 bytes from 10.5.20.1 icmp_seq=1 ttl=63 time=22.429 ms
84 bytes from 10.5.20.1 icmp_seq=2 ttl=63 time=23.721 ms
84 bytes from 10.5.20.1 icmp_seq=3 ttl=63 time=21.576 ms
```
```

```
//adm1
```
adm1> ping 10.5.10.1

84 bytes from 10.5.10.1 icmp_seq=1 ttl=63 time=18.610 ms
84 bytes from 10.5.10.1 icmp_seq=2 ttl=63 time=21.890 ms
84 bytes from 10.5.10.1 icmp_seq=3 ttl=63 time=18.061 ms
```
```
adm1> ping 10.5.10.2

84 bytes from 10.5.10.2 icmp_seq=1 ttl=63 time=26.164 ms
84 bytes from 10.5.10.2 icmp_seq=2 ttl=63 time=24.713 ms
84 bytes from 10.5.10.2 icmp_seq=3 ttl=63 time=22.854 ms
```
```

```

# IV. NAT
## 2. Adressage topologie 4

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `clients` | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node               | `clients`        | `admins`         | `servers`        |
|--------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`  | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`  | `10.5.10.2/24`   | x                | x                |
| `adm1.admins.tp5`  | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5` | x                | x                | `10.5.30.1/24`   |
| `r1`               | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**

- branchez à `eth1` côté Cloud
- côté routeur, il faudra récupérer un IP en DHCP (voir [le mémo Cisco](../../cours/memo/memo_cisco.md))
- vous devriez pouvoir `ping 1.1.1.1`

🌞 **Configurez le NAT**

- référez-vous [à la section NAT du mémo Cisco](../../cours/memo/memo_cisco.md#7-configuration-dun-nat-simple)

🌞 **Test**

- ajoutez une route par défaut (si c'est pas déjà fait)
  - sur les VPCS
  - sur la machine Linux
- configurez l'utilisation d'un DNS
  - sur les VPCS
  - sur la machine Linux
- vérifiez un `ping` vers un nom de domaine

# V. Add a building
## 2. Adressage topologie 5

Les réseaux et leurs VLANs associés :

| Réseau    | Adresse        | VLAN associé |
|-----------|----------------|--------------|
| `clients` | `10.5.10.0/24` | 10           |
| `admins`  | `10.5.20.0/24` | 20           |
| `servers` | `10.5.30.0/24` | 30           |

L'adresse des machines au sein de ces réseaux :

| Node                | `clients`        | `admins`         | `servers`        |
|---------------------|------------------|------------------|------------------|
| `pc1.clients.tp5`   | `10.5.10.1/24`   | x                | x                |
| `pc2.clients.tp5`   | `10.5.10.2/24`   | x                | x                |
| `pc3.clients.tp5`   | DHCP             | x                | x                |
| `pc4.clients.tp5`   | DHCP             | x                | x                |
| `pc5.clients.tp5`   | DHCP             | x                | x                |
| `dhcp1.clients.tp5` | `10.5.10.253/24` | x                | x                |
| `adm1.admins.tp5`   | x                | `10.5.20.1/24`   | x                |
| `web1.servers.tp5`  | x                | x                | `10.5.30.1/24`   |
| `r1`                | `10.5.10.254/24` | `10.5.20.254/24` | `10.5.30.254/24` |

## 3. Setup topologie 5

Vous pouvez partir de la topologie 4. 

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

- de tous les équipements réseau
  - le routeur
  - les 3 switches

🖥️ **VM `dhcp1.client1.tp5`**, déroulez la [Checklist VM Linux](#checklist-vm-linux) dessus

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

- il doit distribuer des IPs aux clients dans le réseau `clients` qui sont branchés au même switch que lui
- sans aucune action manuelle, les clients doivent...
  - avoir une IP dans le réseau `clients`
  - avoir un accès au réseau `servers`
  - avoir un accès WAN
  - avoir de la résolution DNS

🌞  **Vérification**

- un client récupère une IP en DHCP
- il peut ping le serveur Web
- il peut ping `8.8.8.8`
- il peut ping `google.com`
